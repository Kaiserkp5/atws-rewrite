<?php
/*	lib/queries
	Contains all SQL statements used by the ATWS application. Pages that need to execute ANY SQL statement reference this page.
	Statements are organized into categories based on which table they affect. Organizational comments have the form server/database/table
	
	Database credentials have also been added to this file.
*/
class Queries {
	/* ------- -------- ------- ------- -------- ------- ------- Database Credentials ------- -------- ------- ------- -------- ------- ------- */
	public $ServerName = "localhost";
	public $DatabaseName = "test";
	public $LocalHostUsername = "root";
	public $LocalHostPassword = "";
	public $ErrorFile = "C:\Users\kkaiser\Desktop\DBError.txt"; // Error Log Directory
	public $OtherTrustID = '9'; // This value is no longer something that can be modified by users and so has been hardcoded.	
	
	// Connection object
    private $conn;
    
    // Constructor creates the connection object and establishes a database connection.
    function __construct() {
        date_default_timezone_set('America/New_York'); // Eastern time
		
        try {
            $this->conn = new mysqli($this->ServerName, $this->LocalHostUsername, $this->LocalHostPassword, $this->DatabaseName);
        } catch(mysqli_sql_exception $e) {
            echo ("Error 650 - Connection error. " . $e->getMessage());
        }
    }
    
    // Destructor ensures the connection object closes.
    function __destruct() {
        $this->conn = null;
    }
	
	// Logs the given error text in the given error log file. Creates the file if it does not exist.
	public function LogDBError($ErrorMessage) {
		try {
			file_put_contents($this->ErrorFile, $ErrorMessage . "\r\n", FILE_APPEND | LOCK_EX);
		}
		catch (Exception $e) {
			echo "<script type='text/javascript'>alert('Could not write error to log.')</script>";
		}
	}
	
	// This function executes each SQL statement in the given array in a transaction. Expects groups of UPDATE/INSERT/DELETE statements only.
	// Will execute each statement in order of the index (indexes should be ascending integers). Returns a success or failure message. Any error encountered is logged.
	public function ExecuteStatement($SQLarray = array()) {
		ksort($SQLarray); // Make sure the array is sorted.
		
        try {
			$this->conn->beginTransaction();
            
			foreach ($SQLarray as $index => $Query) {
				$LastIndex = $index; // Store this value. If the following line causes an error, we can use this to display the SQL code that caused it.
				$Results = $this->conn->query($Query);
			}
			
			$this->conn->commit();
			return "SUCCESS";
        } catch (\Exception $e) {
			$this->conn->rollback();
			if (isset($LastIndex)) {
				$this->LogDBError($e->getMessage(), "Query was: " . $SQLarray[$LastIndex]);
			} else {
				$this->LogDBError($e->getMessage(), "Calling function: ExecuteStatement");
			}
            
			return "FAILURE";
        }
	}

	// Executes the given query, which should be a single SELECT query. Logs any errors encountered. Returns the Handler object for the caller to parse through.
	public function QueryStatement($QueryText) {
		
		try {
			$QueryResults = $this->conn->query($QueryText);
        } catch (\Exception $e) {
            $this->LogDBError($e->getMessage(), "Calling function: QueryStatement");
        }
		
        return $QueryResults;
	}
	
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Cashbook ------- -------- ------- ------- -------- ------- ------- */
	// Returns the configuration settings for the selected book. Namely, the titles and fee types that appear in the receipt and disbursement columns.
	//	$BookID - ID of the book being examined. TODO: Ignoring this for now and using only cashbook (bookKey = 1).
	public function GetBookConfig($BookID) {
		return $this->QueryStatement("SELECT columnNumber, colHeader FROM tbl_cbColHeaderSetup WHERE bookKey = 1");
	}
	
	// Returns the next sequential check number in the system.
	public function NextCheckNum() {
		return $this->QueryStatement("SELECT MAX(ReceiptOrCheckNumber) + 1 AS CheckNum, COUNT(ReceiptOrCheckNumber) AS CheckCount FROM tbl_cb WHERE IsThisReceipt = 0");
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Cashbook/Disbursements ------- -------- ------- ------- -------- ------- ------- */
	// Gets information needed for the Disburse Payment page.
	//	$LineNum - The line number corresponding to the payment being disbursed.
	//	$ColNum - The column in the payment that is being disbursed. Has the form Col_R_X, where X is the column number (1-10).
	public function GetDisbursementInfo($LineNum, $ColNum) {
		return $this->QueryStatement("SELECT TotalCashDisbursed, $ColNum AS CashReceived, receiptType FROM tbl_cb WHERE cbLineNumber = $LineNum");
	}
	
	// Returns all disbursements that have the given fee type ID and occur on the given receipt.
	public function GetReceiptFees($ReceiptID, $FeeID) {
		$QueryText = "SELECT ReceiptUID, FeeID, PaymentUID, disbursements.Amount, DateDisbursed, disbursements.CheckNum, CauseNum, TaxWarrantNum, payment.Amount AS TotalPayment, DisbursementUID ";
		$QueryText .= "FROM test.disbursements INNER JOIN test.payment ON disbursements.PaymentID = payment.PaymentUID INNER JOIN test.receipt ON receipt.ReceiptUID = payment.ReceiptID ";
		$QueryText .= "WHERE ReceiptUID = $ReceiptID AND FeeID = $FeeID";
		
		return $this->QueryStatement($QueryText);
	}
	
	// Inserts a record for the disbursement of a payment. TODO: Empty items below and Supplied/Additional voucher information?
	public function DisbursePayment($LineNum, $DisbColNum, $Amount, $DateDisbursed, $PayTo, $PayFor, $DisburseCauseNum, $DisburseTaxWarrant, $receiptType) {
		// Subquery determines the next xheck number to use and will be used in the insert statement to determine the next check number at the time the query executes.
		$NextCheckNumSubQuery = "(SELECT MAX(ReceiptOrCheckNumber) + 1 AS CheckNum, COUNT(ReceiptOrCheckNumber) AS CheckCount FROM tbl_cb WHERE IsThisReceipt = 0)";
		
		// Now insert the new record.
		$SQLinsert = "INSERT INTO tbl_cb (cbLineNumber, cbMasterKey, receiptType, ReceiptOrCheckNumber, IsThisReceipt, ReceiptOrCheckNumberDate, user, loc, FromWhomReceivedOrToWhomPaid, ";
		$SQLinsert .= "TotalCashDisbursed, TransID, OnWhatAccount, Col_D_".($DisbColNum - 10).", ReferenceCBLineNumber, creationDate, AccountNumber, CauseNumber, TaxWarrantNumber) VALUES ";
		$SQLinsert .= "((SELECT MAX(cbLineNumber) + 1 FROM tbl_cb), (SELECT cbMasterKey FROM tbl_cb WHERE cbLineNumber = $LineNum), '$receiptType', $NextCheckNumSubQuery, 0, '$DateDisbursed', '";
		$SQLinsert .= $_SESSION['user']."', '".$_SESSION['Location']."', '$PayTo', $Amount, cbLineNumber, '$PayFor', $Amount, $LineNum, '".date("Y-m-d h:i:s")."', (SELECT BankAccountNumber";
		$SQLinsert .= "FROM tbl_BankAccounts WHERE Active = 1), '$DisburseCauseNum', '$DisburseTaxWarrant')";
		
		return $this->ExecuteStatement(array('1' => $SQLinsert));
	}
	
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Cashbook/Display and Navigation ------- -------- ------- ------- -------- ------- ------- */
	
	// Returns the titles of all books in the database.
	public function GetBookTitles() {
		// Open DB connection.
		$conn = new mysqli($this->ServerName, $this->LocalHostUsername, $this->LocalHostPassword, $this->DatabaseName);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		$Results = $conn->query("SELECT bookName FROM tbl_books");
		
		$conn->close(); // Close connection now that we're done querying.
		
		// Organize results.
		$OrganizedResults = array();
		while ($row = $Results->fetch_assoc()) {
			$OrganizedResults[] = $row['BookTitle'];
		}
		
		return $OrganizedResults;
	}
	
	public function AddMonthToBook($BookTitle) {
		// Fetch the most recent month entry for this book.
		$MostRecentEntryResult = $this->QueryStatement("SELECT MAX(ReportingMonth) AS ReportingMonth, ReportingYear FROM test.BookEntries WHERE BookTitle = '" . $BookTitle
													. "' AND ReportingYear = (SELECT MAX(ReportingYear) FROM test.BookEntries WHERE BookTitle = '" . $BookTitle . "')");
		
		// Determine the new entry month/year.
		if (mysqli_num_rows($MostRecentEntryResult) > 0) {
			$row = $MostRecentEntryResult->fetch_assoc();
			$ReportingMonth = ($row['ReportingMonth'] == 12) ? 1 : $row['ReportingMonth'] + 1;
			$ReportingYear = ($row['ReportingMonth'] == 12) ? $row['ReportingYear'] + 1 : $row['ReportingYear'];
			
			// Add the new month.
			return $this->ExecuteStatement(array('1' => "INSERT INTO test.BookEntries (BookTitle, ReportingMonth, ReportingYear, Closed) VALUES ('$BookTitle', $EntryMonth, $EntryYear, 0)"));
			
		} else { // No recent month entries found.
			$ReportingMonth = 1;
			$ReportingYear = date('Y');
			return $this->ExecuteStatement(array('1' => "INSERT INTO test.BookEntries (BookTitle, ReportingMonth, ReportingYear, Closed) VALUES ('$BookTitle', $EntryMonth, $EntryYear, 0)"));
		}
	}
	
	// The following function returns all month entries found in the specified book. TODO: New query returns ONLY cashbook data, ignoring $BookTitle
	public function GetBookEntriesByTitle($BookTitle) {
		return $this->QueryStatement("SELECT SUBSTRING(cbMasterKey, 0, 4) AS ReportingYear, SUBSTRING(cbMasterKey, 4, 2) AS ReportingMonth, cbMonthClosed AS Closed FROM tbl_cbMaster");
	}
	
	// Returns the receipt ID's in the receipting section of the specified cashbook's configuration.
	public function GetReceiptConfiguration($BookTitle) {
		$Query = "SELECT ReceiptLabelCol2, ReceiptLabelCol3, ReceiptLabelCol4, ReceiptLabelCol5, ReceiptLabelCol6, ReceiptLabelCol7, ReceiptLabelCol8, ReceiptLabelCol9, ReceiptLabelCol10, ReceiptLabelCol11";
		$Query .= ", ReceiptValueCol2, ReceiptValueCol3, ReceiptValueCol4, ReceiptValueCol5, ReceiptValueCol6, ReceiptValueCol7, ReceiptValueCol8, ReceiptValueCol9, ReceiptValueCol10, ReceiptValueCol11";
		$Query .= " FROM test.BookConfiguration WHERE BookTitle = '" . $BookTitle . "'";
		
		$Results = $this->QueryStatement($Query);
		
		return $Results->fetch_assoc();
	}
	
	// Returns the receipt ID's in the disburesment section of the specified cashbook's configuration.
	public function GetDisbursementConfiguration($BookTitle) {
		$Query = "SELECT DisburseLabelCol2, DisburseLabelCol3, DisburseLabelCol4, DisburseLabelCol5, DisburseLabelCol6, DisburseLabelCol7, DisburseLabelCol8, DisburseLabelCol9, DisburseLabelCol10, DisburseLabelCol11";
		$Query .= ", DisburseValueCol2, DisburseValueCol3, DisburseValueCol4, DisburseValueCol5, DisburseValueCol6, DisburseValueCol7, DisburseValueCol8, DisburseValueCol9, DisburseValueCol10, DisburseValueCol11";
		$Query .= " FROM test.BookConfiguration WHERE BookTitle = '" . $BookTitle . "'";
		
		// Open DB connection.
		$conn = new mysqli($this->ServerName, $this->LocalHostUsername, $this->LocalHostPassword, $this->DatabaseName);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		$Results = $conn->query($Query);
		
		$conn->close(); // Close connection now that we're done querying.
		
		return $Results->fetch_assoc();
	}
	
	// Returns a FeeID and Amount. This corresponds to the amount of undisbursed money of each fee type from the month prior to the target month.
	public function GetAmountBroughtForward($BookSelected, $TargetMonth) {
		// Get the month prior to the month being viewed.
		$DateArray = explode('-', $TargetMonth);
		$Tmonth = ($DateArray[1] < 10) ? '0' . $DateArray[1] : $DateArray[1];
		$Target = $DateArray[0] . $Tmonth . '1'; // String of the form YYYYMM1
		
		$QueryText = "SELECT cbColBalFwd1, cbColBalFwd2, cbColBalFwd3, cbColBalFwd4, cbColBalFwd5, cbColBalFwd6, cbColBalFwd7, cbColBalFwd8, cbColBalFwd9, cbColBalFwd10 FROM tbl_cbMaster WHERE cbMasterKey = $Target";
		
		return $this->QueryStatement($QueryText);
	}
	
	// Returns the ID for each payment that has undisbursed money that will appear in the above GetAmountBroughtForward query. Also returns the amount remaining to be disbursed on each payment.
	public function GetPaymentsBroughtForward($BookSelected, $TargetMonth, $FeeID) {
		// Get the month prior to the month being viewed.
		$DateArray = explode('-', $TargetMonth);
		$Pyear = ($DateArray[1] == 1) ? $DateArray[0] - 1: $DateArray[0];
		$Pmonth = ($DateArray[1] == 1) ? 12 : $DateArray[1] - 1;
		$PriorMonth = ($Pmonth < 10) ? ($Pyear . "-0" . $Pmonth . "-01") : ($Pyear . "-" . $Pmonth . "-01"); // Returns a string of the form YYYY-MM-01
		
		return "SELECT PayTable.PaymentUID, (PayTable.Amount - IFNULL(DisbTable.Disbursed, 0)) AS Amount FROM (SELECT PaymentUID, Amount FROM test.payment INNER JOIN test.receipt "
			. "ON ReceiptID = ReceiptUID WHERE EntryMonth = '$PriorMonth' AND FeeType = $FeeID) PayTable LEFT JOIN (SELECT SUM(disbursements.Amount) AS Disbursed, disbursements.PaymentID "
			. "FROM disbursements INNER JOIN test.payment ON PaymentID = PaymentUID INNER JOIN test.receipt ON ReceiptID = ReceiptUID WHERE EntryMonth = '$PriorMonth' AND payment.FeeType = $FeeID "
			. "GROUP BY disbursements.PaymentID) DisbTable ON PayTable.PaymentUID = DisbTable.PaymentID WHERE (PayTable.Amount - IFNULL(DisbTable.Disbursed, 0)) > 0";
	}
	
	
	
	// Returns all disburesements made during the specified month that appear in the given book.
	//	$BookTitle - Title of the book we want to search for. TODO: Ignoring this and always return cashbook at the moment.
	//	$TargetMonth - String determining the month we want data for. Has the form: 'YYYY-MM-01'
	public function GetDisbursementDataForBook($BookTitle, $TargetMonth) {
		$QueryText = "SELECT cbLineNumber, ReceiptOrCheckNumber AS CheckNum, ReceiptOrCheckNumberDate AS DateReceived, TaxWarrantNumber, FromWhomReceivedOrToWhomPaid AS ToWhomPaid, OnWhatAccount, ";
		$QueryText .= "TotalCashReceived, TotalCashDisbursed, Col_D_1, Col_D_2, Col_D_3, Col_D_4, Col_D_5, Col_D_6, Col_D_7, Col_D_8, Col_D_9, Col_D_10 ";
		$QueryText .= "FROM tbl_cb WHERE CONCAT(YEAR(ReceiptOrCheckNumberDate), '-', MONTH(ReceiptOrCheckNumberDate), '-01') = '" . $TargetMonth . "' AND IsThisReceipt = 0";
		
		return $this->QueryStatement($QueryText);
	}
	
	
	
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Receipting ------- -------- ------- ------- -------- ------- ------- */
	// Gets all receipt type info. Returns an array indexed by ReceiptTypeID, value is an array of all database values in the receipt type table.
	public function GetReceiptTypeInfo(){
		// Open DB connection.
		$conn = new mysqli($this->ServerName, $this->LocalHostUsername, $this->LocalHostPassword, $this->DatabaseName);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		$ReceiptResults = $conn->query("SELECT TypeName, TypeID FROM test.ReceiptTypes ORDER BY TypeID");
		$FeeResults = $conn->query("SELECT ReceiptTypeID, FeeTypeID, OrderOnEntryForm, OptionalTrust FROM test.ReceiptFees");
		
		$conn->close(); // Close connection now that we're done querying.
		
		// Organize results.
		$OrganizedResults = array();
		while ($row = $ReceiptResults->fetch_assoc()) { // This loop creates an entry for each type, indexed by type ID. The only value filled now is Type Name.
			$OrganizedResults[$row['TypeID']] = array(
								"TypeName" => $row['TypeName'], // Name of the fee type.
								"OptionalFeesA" => array(), "OptionalFeesB" => array(), "OptionalFeesC" => array(), "OptionalFeesD" => array(), // These arrays determine which fees appear in each dropdown.
								"FeeTypes" => array("1" => "0", "2" => "0", "3" => "0", "4" => "0", "5" => "0", "6" => "0", "7" => "0", "8" => "0", "9" => "0") // This array determines the 9 fees appearing directly on the form.
													);
		}
		while ($row = $FeeResults->fetch_assoc()) { // Fill in the remaining values in the array.
			switch ($row['OptionalTrust']) {
				case 'N': // This fee appears on the form directly, not in the optional dropdowns.
					$OrganizedResults[$row['ReceiptTypeID']]["FeeTypes"][$row['OrderOnEntryForm']] = $row['FeeTypeID'];
					
					break;
				case 'A': // This fee appears in dropdown A.
					$OrganizedResults[$row['ReceiptTypeID']]["OptionalFeesA"][$row['FeeTypeID']] = 1;
					
					break;
				case 'B': // This fee appears in dropdown B.
					$OrganizedResults[$row['ReceiptTypeID']]["OptionalFeesB"][$row['FeeTypeID']] = 1;
					
					break;
				case 'C': // This fee appears in dropdown C.
					$OrganizedResults[$row['ReceiptTypeID']]["OptionalFeesC"][$row['FeeTypeID']] = 1;
					
					break;
				case 'D': // This fee appears in dropdown D.
					$OrganizedResults[$row['ReceiptTypeID']]["OptionalFeesD"][$row['FeeTypeID']] = 1;
					
					break;
				default: // There are no other valid options. Log an error.
					$this->LogDBError("Error Code 620: ReceiptFee table entry for RID: ".$row['ReceiptTypeID'].", FID: ".$row['FeeTypeID']." has an invalid state.");
					echo "The database encountered an error. Please check the error log for Error Code 620.";
					break;
			}
		}
		
		return $OrganizedResults;
	}

	// Gets all the database information needed when the Create Receipt page first loads.
	//		$ReceiptType - The type of receipt being loaded, which determines the list of fees that can appear on this receipt.
	public function GetReceiptModelInfo($ReceiptType) {
		// These queries are standardized across multiple functions that request the same info.
		$ReceiptTypesQuery = "SELECT RT.ReceiptTypeDesc, RT.ReceiptType, ST.StubRow FROM tbl_receiptType RT INNER JOIN tbl_Stub_Receipt_Types ST ON RT.ReceiptType = ST.Receipt_Type ORDER BY ReceiptType"; // List of receipt types the user can select.
		$PaymentTypesQuery = "SELECT Payment_Descr, Payment_Type, AllowCheckNumberEntry FROM tbl_PaymentType ORDER BY Payment_Type"; // List of payment types the user can select.
		$NextReceiptQuery = "SELECT MAX(ReceiptOrCheckNumber) + 1 AS RecNum, COUNT(ReceiptOrCheckNumber) AS RecCount FROM tbl_cb WHERE IsThisReceipt = 1"; // Count included only to guarantee there is a row returned.
		$FeesListQuery = "SELECT stubRow, stubRowDescription FROM tbl_stubSetup"; // List of fees as they appear on the page.
		
		// Initialize return array.
		$DBResultsArray = array();
		$DBResultsArray["ReceiptTypesResult"] = $this->QueryStatement($ReceiptTypesQuery); // Result for receipt types.
		$DBResultsArray["PaymentTypesResult"] = $this->QueryStatement($PaymentTypesQuery); // Result for payment types.
		$DBResultsArray["NextReceiptResult"] = $this->QueryStatement($NextReceiptQuery); // Next receipt number, assuming no one else prints first.
		$DBResultsArray["FeesListResult"] = $this->QueryStatement($FeesListQuery); // Query result for FeesListQuery.

		return $DBResultsArray;
	}
	
	// Return a query that will void the given receipt.
	public function VoidReceipt($ReceiptNum) {
		// The following booleans determine the return message of this function.
		$NoReceipt = false; // Boolean is set true if no receipt with the given number can be found.
		$DBError = false; // Boolean is set true if a DB error is encountered.
		
		try {
			$conn = new PDO("mysql:host=".$this->ServerName.";dbname=".$this->DatabaseName, $this->LocalHostUsername, $this->LocalHostPassword);
		} catch (\Exception $e) {
			die("Unable to connect: " . $e->getMessage());
		}
		
		// Now that we have the PDO, insert the records.
		try {			
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			
			// First count the number of rows with the given receipt number.
			$RecHandler = $conn->query("SELECT COUNT(ReceiptUID) AS RecCount FROM test.Receipt WHERE ReceiptUID = $ReceiptNum");
			$RecHandler->setFetchMode(PDO::FETCH_ASSOC);
			$row = $RecHandler->fetch();
			
			if ($row['RecCount'] == 0) {
				$NoReceipt = true;
			} else {
				$conn->beginTransaction();
				
				// If there was a receipt found, update the record.
				$RecHandler = $conn->prepare("UPDATE test.receipt SET Void = 1 WHERE ReceiptUID = $ReceiptNum");
				$RecHandler->execute();
				
				$conn->commit();
			}
		} catch (\Exception $e) {
			$conn->rollback();
			$this->LogDBError($e->getMessage());
			$DBError = true;
		} finally {
			$conn = NULL; // Close connection now that we're done querying.
		}
		
		// Determine the return message for the user.
		if ($DBError) {
			return "Failed - PDO caught an exception.";
		} else {
			return $NoReceipt ? "Could not find a receipt with number: $ReceiptNum" : "Successfully voided receipt number $ReceiptNum";
		}
	}	
	
	// Inserts a new record into the payments table and the cashbook table using the supplied information from the receipt entry form.
	public function CreateReceipt($CauseNum, $TWNum, $ReceivedOf, $ReceiptFor, $PaymentType, $CheckNum, $Notes, $TotalFees, $ReceiptType, $ServiceFee) {
		$PayInsert = "INSERT INTO tbl_Payments (Taxpayer_TID, Liability_Nbr, Warrant_Number, userName, Payment_Date, Payment_Amt, Payment_Type, Check_Number, Payment_Key, PaidBy, PaymentNote) VALUES";
		$PayInsert = "(<Taxpayer_TID>, <Liability_Nbr>, $TWNum, '".$_SESSION['user']."', '".date("Y-m-d h:i:s")."', $TotalFees, '$PaymentType', $CheckNum, <Payment_Key>, '$ReceivedOf', '$Notes')";
		
		$CBInsert = "INSERT INTO tbl_cb (cbLineNumber, receiptType, ReceiptOrCheckNumber, IsThisReceipt, ReceiptOrCheckNumberDate, user, loc, CauseNumber, TaxWarrantNumber, FromWhomReceivedOrToWhomPaid, ";
		$CBInsert .= "TotalCashReceived, stubTotal, TransID, Payment_Type, Check_number, OnWhatAccount, recMemo, receiptSetupKey, stubSetupKey, SheriffCollectionFee) VALUES ";
		$CBInsert .= "((SELECT MAX(cbLineNumber) + 1 FROM tbl_cb), $ReceiptType, (SELECT MAX(ReceiptOrCheckNumber) + 1 FROM tbl_cb WHERE IsThisReceipt = 1), 1, '".date("Y-m-d")."', '".$_SESSION['user'] . "', ";
		$CBInsert .= $_SESSION['Location'].", $CauseNum, $TWNum, '$ReceivedOf', $TotalFees, $TotalFees, (SELECT MAX(cbLineNumber) + 1 FROM tbl_cb), $PaymentType, $CheckNum, '$ReceiptFor', '', 1, 1, $ServiceFee)";
		
		return $this->ExecuteStatement(array('1' => $PayInsert, '2' => $CBInsert));
	}
	
	// Returns the  that occurred in the given book during the given month.
	//	$BookTitle - Title of the book we want to search for. TODO: Ignoring this and always return cashbook at the moment.
	//	$TargetMonth - String determining the month we want data for. Has the form: 'YYYY-MM-01'
	public function GetPaymentDataForBook($BookTitle, $TargetMonth) {
		$QueryText = "SELECT cbLineNumber, ReceiptOrCheckNumber AS ReceiptNum, ReceiptOrCheckNumberDate AS DateReceived, CauseNumber, FromWhomReceivedOrToWhomPaid AS FromWhomReceived, OnWhatAccount, ";
		$QueryText .= "TotalCashReceived, TotalCashDisbursed, Col_R_1, Col_R_2, Col_R_3, Col_R_4, Col_R_5, Col_R_6, Col_R_7, Col_R_8, Col_R_9, Col_R_10 ";
		$QueryText .= "FROM tbl_cb WHERE CONCAT(YEAR(ReceiptOrCheckNumberDate), '-', MONTH(ReceiptOrCheckNumberDate), '-01') = '" . $TargetMonth . "' AND IsThisReceipt = 1";
		
		return $this->QueryStatement($QueryText);
	}
	
	
	
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Setup ------- -------- ------- ------- -------- ------- ------- */
	
	/* ------- -------- ------- ------- -------- ------- ------- Setup/Receipt Types ------- -------- ------- ------- -------- ------- ------- */
	
	// Overwrites the database values for the receipt type number in the given model. Returns a success or failure message.
	public function EditReceiptType($NewTypeName, $SelectedReceipt, $NewFees, $NewOPA, $NewOPB, $NewOPC, $NewOPD) {
		// The following booleans determine the return message of this function.
		$NoReceiptType = false; // Boolean is set true if no receipt with the given number can be found.
		$DBError = false; // Boolean is set true if a DB error is encountered.
		
		// Create PDO.
		try {
			$conn = new PDO("mysql:host=".$this->ServerName.";dbname=".$this->DatabaseName, $this->LocalHostUsername, $this->LocalHostPassword);
		} catch (\Exception $e) {
			die("Unable to connect: " . $e->getMessage());
		}
		
		// Now that we have the PDO, insert the records.
		try {			
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$conn->beginTransaction();
			
			// First count the number of rows with the given receipt type ID number.
			$RecHandler = $conn->query("SELECT COUNT(TypeID) AS RecCount FROM test.ReceiptTypes WHERE TypeID = " . $SelectedReceipt);
			$RecHandler->setFetchMode(PDO::FETCH_ASSOC);
			$row = $RecHandler->fetch();
			
			if ($row['RecCount'] == 0) {
				$NoReceiptType = true;
			} else { // If there was a receipt type found, update it.
				// First update the type name in the ReceiptTypes table.
				$RecHandler = $conn->prepare("UPDATE test.receipttypes SET TypeName = '" . $NewTypeName . "' WHERE TypeID = " . $SelectedReceipt);
				$RecHandler->execute();
				
				/*** Next update the ReceiptFees table with the valid Fee Types for this ReceiptType. ***/
				// Delete all entries for this receipt before hand and then insert the new relationships.
				$RecHandler = $conn->query("DELETE FROM test.receiptfees WHERE ReceiptTypeID = " . $SelectedReceipt);
				if ($RecHandler->errorCode() != 0) // If true, an error occurred while deleting.
					throw new Exception($RecHandler->errorInfo());
					
				$RecHandler = $conn->prepare("INSERT INTO test.receiptfees (ReceiptTypeID, FeeTypeID, OrderOnEntryForm, OptionalTrust) "
											."VALUES (:ReceiptTypeID, :FeeTypeID, :OrderOnEntryForm, :OptionalTrust)");

				// First loop adds the 9 Fees.
				foreach ($NewFees as $Order => $ID) { // Array (Order On Entry -> ID) of the 9 fees that appear on this receipt type.
					if ($ID > 0) { // Empty dropdowns on the entry form have a value of 0. Don't put in a record for these.
						$ParamArray = array(":ReceiptTypeID" => $SelectedReceipt, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => $Order, ":OptionalTrust" => 'N');
						$RecHandler->execute($ParamArray);
					}
				}
				// These next four loops add the optional fees for dropdowns A through D.
				foreach ($NewOPA as $ID => $Bool) { // Array (ID -> Bool: Used on this receipt?) of all fees appearing in the Optional Fees field of this receipt type.
					if ($Bool) {
						$ParamArray = array(":ReceiptTypeID" => $SelectedReceipt, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'A');
						$RecHandler->execute($ParamArray);
					}
				}
				foreach ($NewOPB as $ID => $Bool) { // B Loop, see above note.
					if ($Bool) {
						$ParamArray = array(":ReceiptTypeID" => $SelectedReceipt, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'B');
						$RecHandler->execute($ParamArray);
					}
				}
				foreach ($NewOPC as $ID => $Bool) { // C Loop, see above note.
					if ($Bool) {
						$ParamArray = array(":ReceiptTypeID" => $SelectedReceipt, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'C');
						$RecHandler->execute($ParamArray);
					}
				}
				foreach ($NewOPD as $ID => $Bool) { // D Loop, see above note.
					if ($Bool) {
						$ParamArray = array(":ReceiptTypeID" => $SelectedReceipt, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'D');
						$RecHandler->execute($ParamArray);
					}
				}
				/*** Finished updating the ReceiptFees table. ***/
				
				$conn->commit();
			}
		} catch (\Exception $e) {
			$conn->rollback();
			$this->LogDBError($e->getMessage());
			$DBError = true;
		} finally {
			$conn = NULL; // Close connection now that we're done querying.
		}
		
		// Determine the return message for the user.
		if ($DBError) {
			return "Failed - PDO caught an exception.";
		} else {
			return $NoReceiptType ? "Could not find a receipt type with ID: " . $SelectedReceipt : "Successfully updated receipt type: " . $NewTypeName;
		}
	}
	
	// Creates a new receipt type in the database with the values in the given model. Returns a success or failure message.
	public function CreateReceiptType($NewTypeName, $NewFees, $NewOPA, $NewOPB, $NewOPC, $NewOPD) {
		$DBError = false; // Boolean is set true if a DB error is encountered.
		
		// Create PDO.
		try {
			$conn = new PDO("mysql:host=".$this->ServerName.";dbname=".$this->DatabaseName, $this->LocalHostUsername, $this->LocalHostPassword);
		} catch (\Exception $e) {
			die("Unable to connect: " . $e->getMessage());
		}
		
		// Now that we have the PDO, insert the records.
		try {			
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$conn->beginTransaction();
			
			// First create a record in the ReceiptTypes table.
			$RecHandler = $conn->prepare("INSERT INTO test.receipttypes (TypeName) VALUES (:TypeName)");
			$RecHandler->execute(array("TypeName" => $NewTypeName));
			
			// Now get the ID of the record we just inserted.
			$RecHandler = $conn->query("SELECT MAX(TypeID) AS IDval FROM test.ReceiptTypes");
			$RecHandler->setFetchMode(PDO::FETCH_ASSOC);
			$row = $RecHandler->fetch();
			$MaxID = $row['IDval'];
			
			/*** Next update the ReceiptFees table with the valid Fee Types for this ReceiptType. ***/
			$RecHandler = $conn->prepare("INSERT INTO test.receiptfees (ReceiptTypeID, FeeTypeID, OrderOnEntryForm, OptionalTrust) "
										."VALUES (:ReceiptTypeID, :FeeTypeID, :OrderOnEntryForm, :OptionalTrust)");

			// First loop adds the 9 Fees.
			foreach ($NewFees as $Order => $ID) { // Array (Order On Entry -> ID) of the 9 fees that appear on this receipt type.
				$ParamArray = array(":ReceiptTypeID" => $MaxID, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => $Order, ":OptionalTrust" => 'N');
				$RecHandler->execute($ParamArray);
			}
			// These next four loops create entries for the values appearing in dropdowns A through D.
			foreach ($NewOPA as $ID => $Bool) { // Array (ID -> Bool: Used on this receipt?) of all fees appearing in the Optional Fees field of this receipt type.
				if ($Bool) {
					$ParamArray = array(":ReceiptTypeID" => $MaxID, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'A');
					$RecHandler->execute($ParamArray);	
				}
			}
			foreach ($NewOPB as $ID => $Bool) { // B Loop, see above note.
				if ($Bool) {
					$ParamArray = array(":ReceiptTypeID" => $MaxID, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'B');
					$RecHandler->execute($ParamArray);	
				}
			}
			foreach ($NewOPC as $ID => $Bool) { // C Loop, see above note.
				if ($Bool) {
					$ParamArray = array(":ReceiptTypeID" => $MaxID, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'C');
					$RecHandler->execute($ParamArray);	
				}
			}
			foreach ($NewOPD as $ID => $Bool) { // D Loop, see above note.
				if ($Bool) {
					$ParamArray = array(":ReceiptTypeID" => $MaxID, ":FeeTypeID" => $ID, ":OrderOnEntryForm" => 0, ":OptionalTrust" => 'D');
					$RecHandler->execute($ParamArray);	
				}
			}
			/*** Finished updating the ReceiptFees table. ***/
			
			$conn->commit();
			
		} catch (\Exception $e) {
			$conn->rollback();
			$this->LogDBError($e->getMessage());
			$DBError = true;
		} finally {
			$conn = NULL; // Close connection now that we're done querying.
		}
		
		return $DBError ? "Failed - PDO caught an exception." : "Successfully created receipt type: " . $NewTypeName;
	}
	
	// Deletes the given receipt type.
	public function DeleteReceiptType($TypeID) {
		$DBError = false; // Will be set to TRUE if a DB error is found. Determines the validation message returned.
		
		// Create PDO.
		try {
			$conn = new PDO("mysql:host=".$this->ServerName.";dbname=".$this->DatabaseName, $this->LocalHostUsername, $this->LocalHostPassword);
		} catch (\Exception $e) {
			die("Unable to connect: " . $e->getMessage());
		}
		
		// Now that we have the PDO, insert the records.
		try {			
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$conn->beginTransaction();
			
			// Delete the receipt type from the ReceiptTypes table.
			$RecHandler = $conn->prepare("DELETE FROM test.ReceiptTypes WHERE TypeID = " . $TypeID);
			$RecHandler->execute();
			
			// Delete the entries in the ReceiptFees table linking the deleted receipt type to fee types.
			$FeeHandler = $conn->prepare("DELETE FROM test.ReceiptFees WHERE ReceiptTypeID = " . $TypeID);
			$FeeHandler->execute();
			
			$conn->commit();
		} catch (\Exception $e) {
			$conn->rollback();
			$this->LogDBError($e->getMessage());
			$DBError = true;
		} finally {
			$conn = NULL; // Close connection now that we're done querying.
		}
		
		return $DBError ? "Failed to delete receipt type: PDO caught an exception." : "Successfully deleted receipt type.";
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Setup/Cashbook Configuration ------- -------- ------- ------- -------- ------- ------- */
	// Edits the specified cashbooks configuration.
	public function EditCashbookConfig($Book, $ConfigName, $Receipts, $Disbursements) {
		
		// Create PDO.
		try {
			$conn = new PDO("mysql:host=".$this->ServerName.";dbname=".$this->DatabaseName, $this->LocalHostUsername, $this->LocalHostPassword);
		} catch (\Exception $e) {
			die("Unable to connect: " . $e->getMessage());
		}
		
		// Now that we have the PDO, insert the records.
		try {			
			$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			$conn->beginTransaction();
			
			// Execute an UPDATE statement for the selected book configuration.
			$ConfigHandler = $conn->prepare("UPDATE test.BookConfiguration SET ReceiptLabelCol2, ReceiptLabelCol3, ReceiptLabelCol4, ReceiptLabelCol5, ReceiptLabelCol6, ReceiptLabelCol7, ReceiptLabelCol8, ReceiptLabelCol9, "
											."ReceiptLabelCol10, ReceiptLabelCol11, ReceiptValueCol2, ReceiptValueCol3, ReceiptValueCol4, ReceiptValueCol5, ReceiptValueCol6, ReceiptValueCol7, ReceiptValueCol8, "
											."ReceiptValueCol9, ReceiptValueCol10, ReceiptValueCol11, DisburseLabelCol2, DisburseLabelCol3, DisburseLabelCol4, DisburseLabelCol5, DisburseLabelCol6, DisburseLabelCol7, "
											."DisburseLabelCol8, DisburseLabelCol9, DisburseLabelCol10, DisburseLabelCol11, DisburseValueCol2, DisburseValueCol3, DisburseValueCol4, DisburseValueCol5, DisburseValueCol6, "
											."DisburseValueCol7, DisburseValueCol8, DisburseValueCol9, DisburseValueCol10, DisburseValueCol11 WHERE BookTitle = " . $Book);
			$ConfigHandler->execute();
			
			$conn->commit();
		} catch (\Exception $e) {
			$conn->rollback();
			$this->LogDBError($e->getMessage());
			$DBError = true;
		} finally {
			$conn = NULL; // Close connection now that we're done querying.
		}
		
		return "";
	}
	
	
	
	
	
	
	
	/* ------- -------- ------- ------- -------- ------- ------- General ------- -------- ------- ------- -------- ------- ------- */
	
	// Returns all Fee Types as ID-Name pairs. Used primarily to generate select-tags.
	public function GetFeeTypes() {
		// Open DB connection.
		$conn = new mysqli($this->ServerName, $this->LocalHostUsername, $this->LocalHostPassword, $this->DatabaseName);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		$Results = $conn->query("SELECT FeeTypeID, FeeTitle FROM test.feetype");
		
		$conn->close(); // Close connection now that we're done querying.
		
		// Organize results.
		$OrganizedResults = array();
		while ($row = $Results->fetch_assoc()) {
			$OrganizedResults[$row['FeeTypeID']] = $row['FeeTitle'];
		}
		
		return $OrganizedResults;
	}
}