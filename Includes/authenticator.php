<?php
require_once("environment.php");
require_once("Queries.php");

// Create session for authentication.
session_start();
if ((!isset($_SESSION['LoggedIn'])) || $_SESSION['LoggedIn'] !== 'TRUE') { // User is not logged in. Begin authentication process.
	if ( isset($_POST['username']) && isset($_POST['password']) ) { // Login attempted.
		if ($_POST['username'] == 'bob' && $_POST['password'] == 'password') { // Authenticate user.
			// Authentication okay, setup session and redirect to index page.
			$_SESSION['user'] = $_POST['username'];
			$_SESSION['LoggedIn'] = 'TRUE';
			$_SESSION['Location'] = 'KLM';
			header("Location: http://" . $config['webhost'] . "/dev/ATWS/index.php");
			exit();
		} else { // Authentication didn't work, reload login form.
			header("Location: http://" . $config['webhost'] . "/dev/ATWS/login.php");
			exit();
		}
	} else { // No post variables, user must have been redirected here after trying to access a page without being logged in. Redirect to login page.
		header("Location: http://" . $config['webhost'] . "/dev/ATWS/login.php");
		exit();
	}
}
?>
