<?php
/*
 This file acts as a standard MVC controller for the Cashbook configuration setup page.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

/* ------- Controller functionality. ------- */

// Load the model.
require_once("CashbookModel.php");

// Determine whether a command was entered and take appropriate action.
if (isset($_POST['command'])) {
	// Take action based on the command that was given.
	switch ($_POST['command']) {
		case "Change":
			$ParamArray = array();
			$ParamArray['SelectedConfig'] = $_POST['ConfigSelect'];
			$model = new CashbookModel($ParamArray);
			
			break;
		case "Save Changes":
			// Create the model.
			$ParamArray = array();
			$ParamArray['SelectedConfig'] = $_POST['ConfigSelect'];
			$ParamArray['ConfigName'] = $_POST['ConfigName'];
			$ParamArray['Receipts'] = $_POST[''];
			$ParamArray['Disbursements'] = $_POST[''];
			$model = new CashbookModel($ParamArray);
			
			// Validate user input.
			$ValMsg = $model->IsValid();
			
			if ($ValMsg === 'VALID') {
				// Attempt the update.
				$DBMsg = $model->SaveChanges();
				
				if ($DBMsg === 'SUCCESS') {
					$model->DisplayMessage("Changes saved successfully.");
				} else { // Report database failure.
					$model->DisplayMessage("Database failed to save changes.");
				}
				
			} else { // Report the failure.
				$model->DisplayMessage($ValMsg);
			}
			
			break;
		default:
			echo "Error 640 - Unrecognized command: " . $_POST['command'];
			break;
	}
	
} else { // If no command was entered, do no work and just let the page load.
	$ParamArray = array();
	$model = new CashbookModel($ParamArray);
}


/* ------- Load the page. ------- */
// Create the page template.
require_once('../lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "Cashbook Configuration", 'ContentBody' => REALPATH(DIRNAME(__FILE__))."/CashbookConfig.html", 'FooterMedia' => "",
								  'Copyright' => "Copyright (c) Lieberman Technologies, LLC.", 'ScriptFile' => REALPATH(DIRNAME(__FILE__))."/CashbookConfig.script"));
    require "../lib/layout.php";
    exit;
}
?>