<?php
/*
 This file acts as a standard MVC model for the Receipt Type setup page.
 */
require_once "../../Includes/Queries.php";
 
class ReceiptModel {	
	/* ------- -------- ------- ------- -------- ------- ------- Database variables. ------- -------- ------- ------- -------- ------- ------- */
	private $ReceiptTypes = array(); // Array contains each receipt type. Indexed by ReceiptTypeID, values are a subarray containing all Receipt Type elements.
	private $FeeTypes = array(); // Array contains each fee type. FeeTypeID => FeeTypeName
	public $ValMsg = "none"; // This is set with a validation message to be displayed to the user whenever they submit an edit to some receipt type.
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Model variables. ------- -------- ------- ------- -------- ------- ------- */
	public $SelectedReceipt; // ReceiptTypeID of the Receipt Type currently selected, if any.
	
	// The following model variables are used to store values for a new receipt type when the user creates one.
	public $NewTypeName;
	public $NewFees;
	public $NewOPA;
	public $NewOPB;
	public $NewOPC;
	public $NewOPD;
	
	/* ------- -------- ------- ------- -------- ------- ------- Model functions ------- -------- ------- ------- -------- ------- ------- */
	// Constructor sets any value given and retrieves database values.
	function __construct($ParamArray = array()) {
		/* Set each parameter in the array. */
		foreach($ParamArray as $key=>$value) {
			$this->$key = $value;
		}
		
		// Grab necessary information from the database.
		$Query = new Queries();
		$this->ReceiptTypes = $Query->GetReceiptTypeInfo(); // Get info on all receipt types in the database.
		$this->FeeTypes = $Query->GetFeeTypes(); // Get all fee types in the database as ID-Name pairs.
	}
	
	// Returns the requested value for the selected Receipt Type from the receipt type array.
	//		$ValueName - Name of the value to find.
	//		$FeeType - Determines whether we look up this value in the ReceiptTypes arrays or one of it's subarrays. Default to primary array.
	public function LookupValue($ValueName, $FeeType = 'none') {
		if ($FeeType === 'Fee') {
			return (empty($this->SelectedReceipt)) ? "" :  $this->ReceiptTypes[$this->SelectedReceipt]['FeeTypes'][$ValueName];
		} else {
			return (empty($this->SelectedReceipt)) ? "" :  $this->ReceiptTypes[$this->SelectedReceipt][$ValueName];
		}
	}
	
	// Returns a boolean indicating whether the specified optional trust (A-D) is used.
	//	$TargetTrust - Single character, A-D, indicating which optional fee is being questioned.
	public function IsOptionalTrustUsed($TargetTrust) {
		return (empty($this->SelectedReceipt)) ? false : count($this->ReceiptTypes[$this->SelectedReceipt]['OptionalFees' . $TargetTrust]) > 0;
		}
	
	// Returns "VALID" if this model is currently valid and an error message otherwise.
	public function IsValid() {
		// Make sure receipt number is numeric.
		return (is_numeric($this->ReceiptNum)) ? "VALID" : "Receipt Number must be numeric.";
	}
	
	// Overwrites the database values for the receipt type number in this model.
	public function SaveChanges() {
		$Query = new Queries();
		return $Query->EditReceiptType($this->NewTypeName, $this->SelectedReceipt, $this->NewFees, $this->NewOPA, $this->NewOPB, $this->NewOPC, $this->NewOPD);
	}
	
	// Creates a new receipt type in the database with the values currently in this model.
	public function Create() {
		$Query = new Queries();
		return $Query->CreateReceiptType($this->NewTypeName, $this->NewFees, $this->NewOPA, $this->NewOPB, $this->NewOPC, $this->NewOPD);
	}
	
	// Creates a new receipt type in the database with the values currently in this model.
	public function DeleteRecord() {
		$Query = new Queries();
		return $Query->DeleteReceiptType($this->SelectedReceipt);
	}
	
	// Displays the given validation message.
	public function DisplayMessage($Message) {
		$this->ValMsg = $Message;
	}
	
	// Standard GETTER for the Fee Types database result stored in this model.
	public function GetFeeTypeList() {
		return $this->FeeTypes;
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Display functions ------- -------- ------- ------- -------- ------- ------- */
	
	// Returns HTML code to generate an option tag for each receipt type in the ReceiptTypes array.
	//		SelectType - Boolean. If TURE, the option tag with ID == $this->SelectedReceipt will have the 'selected' tag added to it.
	public function GetReceiptTypeOptionTags($SelectType) {
		$HTML = "";
		
		foreach($this->ReceiptTypes as $TypeID => $ValArray) {
			$HTML .= "<option value='$TypeID'" . (($TypeID == $this->SelectedReceipt) ? " selected" : "") . ">" . $ValArray['TypeName'] . "</option>";
		}
		
		return $HTML;
	}
	
	// Returns HTML code to generate an option tag for each of the Fee Types in the database.
	//		$SelectID - If provided, causes a 'selected' tag to be added to the option tag with this ID.
	public function GetFeeTypeOptionTags($SelectType) {
		$HTML = "";
		
		foreach($this->FeeTypes as $TypeID => $TypeName) {
			if ($TypeID == $SelectType) {
				$HTML .= "<option value='$TypeID' selected>" . $TypeName . "</option>";
			} else {
				$HTML .= "<option value='$TypeID'>" . $TypeName . "</option>";
			}
		}
		
		return $HTML;
	}
	
	// Returns HTML code to generate the data rows in the Optional/Other Trust table.
	// This table has two Fee Types per row. Each Fee Type is repeated twice - once with a checkbox
	// that determines whether the Fee will be displayed on Other Trust, one likewise for Optional Trust.
	public function DisplayTrustRows() {
		$HTML = "";
		$counter = 1; // Will be used to keep track of which column we're on in each row.
		$LoadError = false; // Will be set to TRUE if an error is encountered.
		
		$NextASegment; // These fields store HTML segments temporarily before adding them to $HTML.
		$NextBSegment;
		$NextCSegment;
		$NextDSegment;
		foreach ($this->FeeTypes as $TypeID => $TypeName) {
			// The following booleans will be TRUE if the current $TypeID appears in the corresponding dropdown.
			$InOptionalA = (empty($this->SelectedReceipt)) ? false : array_key_exists($TypeID, $this->ReceiptTypes[$this->SelectedReceipt]['OptionalFeesA']);
			$InOptionalB = (empty($this->SelectedReceipt)) ? false : array_key_exists($TypeID, $this->ReceiptTypes[$this->SelectedReceipt]['OptionalFeesB']);
			$InOptionalC = (empty($this->SelectedReceipt)) ? false : array_key_exists($TypeID, $this->ReceiptTypes[$this->SelectedReceipt]['OptionalFeesC']);
			$InOptionalD = (empty($this->SelectedReceipt)) ? false : array_key_exists($TypeID, $this->ReceiptTypes[$this->SelectedReceipt]['OptionalFeesD']);
			
			switch ($counter) {
				case 1: // Each dropdown section has two columns. Create the entry for the first column of the current row here.
					$NextASegment = "<tr><td><input type='checkbox' name='OPA-$TypeID'" . ($InOptionalA ? " checked" : "") . " />&nbsp; " . $TypeName . "</td>";
					$NextBSegment = "<td class='left-border'><input type='checkbox' name='OPB-$TypeID'" . ($InOptionalB ? " checked" : "") . " />&nbsp; " . $TypeName . "</td>";
					$NextCSegment = "<td class='left-border'><input type='checkbox' name='OPC-$TypeID'" . ($InOptionalC ? " checked" : "") . " />&nbsp; " . $TypeName . "</td>";
					$NextDSegment = "<td class='left-border'><input type='checkbox' name='OPD-$TypeID'" . ($InOptionalD ? " checked" : "") . " />&nbsp; " . $TypeName . "</td>";
					$counter++;
					break;
					
				case 2: // Each dropdown section has two columns. Create the entry for the second column of the current row here and append both entries created.
					$HTML .= $NextASegment . "<td><input type='checkbox' name='OPA-$TypeID'" . ($InOptionalA ? " checked" : "") . " />&nbsp; " . $TypeName . "</td>";
					$HTML .= $NextBSegment . "<td><input type='checkbox' name='OPB-$TypeID'" . ($InOptionalB ? " checked" : "") . " />&nbsp; " . $TypeName . "</td>";
					$HTML .= $NextCSegment . "<td><input type='checkbox' name='OPC-$TypeID'" . ($InOptionalC ? " checked" : "") . " />&nbsp; " . $TypeName . "</td>";
					$HTML .= $NextDSegment . "<td><input type='checkbox' name='OPD-$TypeID'" . ($InOptionalD ? " checked" : "") . " />&nbsp; " . $TypeName . "</td></tr>";
					$counter--;
					break;

				default: // Something really broke.
					$HTML = "Error loading table. Error code: 630";
					$LoadError = true;
					break;
			}
			
			if ($LoadError) { break; } // Break the loop if an error was detected.
		}
		
		// Check the counter variable to determine whether there is another item to add. Since items get added to the HTML string
		// only on EVEN intervals in the above loop, one item will be remaining if there are an ODD number of receipt fees.
		if ($counter == 2) {
			$HTML .= $NextASegment . "<td></td>" . $NextBSegment . "<td></td>" . $NextCSegment . "<td></td>" . $NextDSegment;
		}
		
		return $HTML;
	}
}
?>