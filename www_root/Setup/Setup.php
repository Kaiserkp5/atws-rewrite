<?php
/*
 This file acts as a standard MVC controller for the ATWS setup page, allowing Receipt Types and Cashbook Configurations to be defined.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

// Create the page template.
require_once('../lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "ATWS Setup", 'ContentBody' => __FILE__, 'FooterMedia' => "", 'Copyright' => "Copyright (c) Lieberman Technologies, LLC."));
    require "../lib/layout.php";
    exit;
}
?>
	<section>
		<div class="body-wrapper" id="wrapper">
			<div class="row" align="center">
				<div class="large-12 columns">
					<h3><?php echo $TPL->PageTitle; ?></h3>
				</div>
			</div>
			<div class="row" align="center">
				<div class="large-12 columns">
					<table class="sysmenu">
						<tr><td><a href="ReceiptType.php">Create/Modify Receipt Type</a></td></tr>
						<tr><td><a href="CashbookConfig.php">Create/Modify Cashbook Configuration</a></td></tr>
					</table>
				</div>
			</div>
		</div>
	</section>