<?php
/*
 This file acts as a standard MVC controller for the Receipt Type setup page.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

/* ------- Controller functionality. ------- */

// Load the model.
require_once("ReceiptModel.php");

// Determine whether a command was entered and take appropriate action.
if (isset($_POST['command'])) {
	
	// First, grab all the values that were POSTED.
	$ParamArray = array();
	$ParamArray['SelectedReceipt'] = $_POST['TypeID'];
	$ParamArray['NewTypeName'] = $_POST['TypeName'];
	$model = new ReceiptModel($ParamArray);
	
	// The following fields are a little more complicated. NewFees is an array of the 9 fee types that appear on the form for the receipt type.
	// NewOP and NewOT are an array determining which fees will appear in the optional trust and other trust dropdowns. These two arrays each have an
	// entry for every fee type in the database. The corresponding value is a boolean indicating whether this fee type will appear on this receipt type.
	$NewFees = array("1" => $_POST['FeeType1'], "2" => $_POST['FeeType2'], "3" => $_POST['FeeType3'], "4" => $_POST['FeeType4'],
					 "5" => $_POST['FeeType5'], "6" => $_POST['FeeType6'], "7" => $_POST['FeeType7'], "8" => $_POST['FeeType8'], "9" => $_POST['FeeType9']);
	$NewOPA = array();
	$NewOPB = array();
	$NewOPC = array();
	$NewOPD = array();
	foreach ($model->GetFeeTypeList() as $TypeID => $TypeName) {
		$NewOPA["$TypeID"] = (isset($_POST["OPA-$TypeID"])) ? 1 : 0;
		$NewOPB["$TypeID"] = (isset($_POST["OPB-$TypeID"])) ? 1 : 0;
		$NewOPC["$TypeID"] = (isset($_POST["OPC-$TypeID"])) ? 1 : 0;
		$NewOPD["$TypeID"] = (isset($_POST["OPD-$TypeID"])) ? 1 : 0;
	}
	$model->NewFees = $NewFees;
	$model->NewOPA = $NewOPA;
	$model->NewOPB = $NewOPB;
	$model->NewOPC = $NewOPC;
	$model->NewOPD = $NewOPD;
	
	// Now that we have all the input, take action based on the command that was given.
	if ($_POST['command'] === "Save") { // User is saving the changes made to the currently loaded receipt type.
		$ValMsg = $model->SaveChanges(); // Saves changes, returns a success or failure message.
		$model->DisplayMessage($ValMsg);
		
		// Reload the model.
		$ParamArray = array();
		$ParamArray['SelectedReceipt'] = $_POST['TypeID'];
		$model = new ReceiptModel($ParamArray);
	
	} else if ($_POST['command'] === "Create") { // Create a receipt type based on the input the user has entered.
		$ValMsg = $model->Create(); // Creates DB records, returns a success or failure message.
		$model->DisplayMessage($ValMsg);
		
		// Reload the model.
		$ParamArray = array();
		$model = new ReceiptModel($ParamArray);
		
	} else if ($_POST['command'] === "Delete") { // Delete this item.
		$ValMsg = $model->DeleteRecord(); // Deletes DB records, returns a success or failure message.
		$model->DisplayMessage($ValMsg);
		
		// Reload the model.
		$ParamArray = array();
		$model = new ReceiptModel($ParamArray);
		
	} else if ($_POST['command'] === "Reload") { // Form is being reloaded. No additional work necessary.
		
	} else {
		echo "Error 610 - Unrecognized command.";
	}
} else { // If no command was entered, do no work and just let the page load.
	$ParamArray = array();
	$model = new ReceiptModel($ParamArray);
}


/* ------- Load the page. ------- */
// Create the page template.
require_once('../lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "Receipt Type", 'ContentBody' => REALPATH(DIRNAME(__FILE__)) . "/ReceiptType.html", 'FooterMedia' => "",
								  'Copyright' => "Copyright (c) Lieberman Technologies, LLC.", 'ScriptFile' => REALPATH(DIRNAME(__FILE__)) . "/ReceiptType.script"));
    require "../lib/layout.php";
    exit;
}
?>