<?php
/*
 This file acts as a standard MVC model for the Cashbook Configuration setup page.
 */
require_once "../../Includes/Queries.php";
 
class CashbookModel {	
	/* ------- -------- ------- ------- -------- ------- ------- Database variables. ------- -------- ------- ------- -------- ------- ------- */
	private $Receipts = array(); // Array contains each receipt ID for the selected configuration.
	private $Disbursements = array(); // Array contains each disbursement ID for the selected configuration.
	private $FeeTypes = array(); // Array contains each Fee Type. Fee Type ID => Fee Type Name
	private $Books; // Array contains each book in the database. Book ID => Book Title
	public $ValMsg = "none"; // This is set with a validation message to be displayed to the user whenever they submit an edit to some receipt type.
	
	/* ------- -------- ------- ------- -------- ------- ------- Model variables. ------- -------- ------- ------- -------- ------- ------- */
	public $SelectedConfig; // ID of the book configuration currently selected, if any.
	public $ConfigName; // Used to store new values for configuration names as entered by the user.
	
	// The following model variables are used to store values for a new receipt type when the user creates one.
	public $NewReceipts;
	public $NewDisbursements;
	
	/* ------- -------- ------- ------- -------- ------- ------- Model functions ------- -------- ------- ------- -------- ------- ------- */
	// Constructor sets any value given and retrieves database values.
	function __construct($ParamArray = array()) {
		/* Set each parameter in the array. */
		foreach($ParamArray as $key=>$value) {
			$this->$key = $value;
		}
		
		// Grab necessary information from the database.
		$Query = new Queries();
		$this->FeeTypes = $Query->GetFeeTypes();
		$this->Books = $Query->GetBookTitles();
		
		// If a configuration is selected, load it's information.
		if (isset($this->SelectedConfig)) {
			$this->Receipts = $Query->GetReceiptConfiguration($this->SelectedConfig);
			$this->Disbursements = $Query->GetDisbursementConfiguration($this->SelectedConfig);
		}
	}
	
	// Returns "VALID" if this model is currently valid and an error message otherwise.
	public function IsValid() {
		
		return 'VALID';
	}
	
	// Overwrites the database values for the receipt type number in this model.
	public function SaveChanges() {
		$Query = new Queries();
		return $Query->EditCashbookConfig($this->SelectedConfig, $this->ConfigName, $this->Receipts, $this->Disbursements);
	}
	
	// Displays the given validation message.
	public function DisplayMessage($Message) {
		$this->ValMsg = $Message;
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Display functions ------- -------- ------- ------- -------- ------- ------- */
	// Returns HTML code to create a select list containing all of the existing cashbook configurations in the database.
	public function DisplayConfigSelect() {
		$HTML = "<select name='ConfigSelect' style='width:25%' onchange=\"SubmitCommand('Change')\"><option value=''></option>";
		
		foreach ($this->Books as $ID => $Val) {
			$HTML .= (isset($this->SelectedConfig) && $this->SelectedConfig == $Val) ? "<option value='$Val' selected>$Val</option>" : "<option value='$Val'>$Val</option>";
		}
		
		$HTML .= "</select>";
		
		return $HTML;
	}
	
	// Returns HTML code displaying a table row containing the 10 receipt labels set in the receipt section for this configuration.
	public function DisplayReceiptLabels() {
		$HTML = "";
		
		if (isset($this->SelectedConfig)) { // A configuration is being displayed, show the saved data.
			$HTML .= "<tr><td>Labels:</td><td><textarea name='RL2'/>".$this->Receipts['ReceiptLabelCol2']."</textarea></td><td><textarea name='RL3'/>".$this->Receipts['ReceiptLabelCol3']."</textarea></td><td><textarea name='RL4'/>".$this->Receipts['ReceiptLabelCol4']."</textarea></td>";
			$HTML .= "<td><textarea name='RL5'/>".$this->Receipts['ReceiptLabelCol5']."</textarea></td><td><textarea name='RL6'/>".$this->Receipts['ReceiptLabelCol6']."</textarea></td><td><textarea name='RL7'/>".$this->Receipts['ReceiptLabelCol7']."</textarea></td><td><textarea name='RL8'/>".$this->Receipts['ReceiptLabelCol8']."</textarea></td>";
			$HTML .= "<td><textarea name='RL9'/>".$this->Receipts['ReceiptLabelCol9']."</textarea></td><td><textarea name='RL10'/>".$this->Receipts['ReceiptLabelCol10']."</textarea></td><td><textarea name='RL11'/>".$this->Receipts['ReceiptLabelCol11']."</textarea></td></tr>";
		} else {
			$HTML .= "<tr><td>Labels:</td><td><textarea name='NewRL2'></textarea></td><td><textarea name='NewRL3'></textarea></td><td><textarea name='NewRL4'></textarea></td><td><textarea name='NewRL5'></textarea></td>";
			$HTML .= "<td><textarea name='NewRL6'></textarea></td><td><textarea name='NewRL7'></textarea></td><td><textarea name='NewRL8'></textarea></td><td><textarea name='NewRL9'></textarea></td>";
			$HTML .= "<td><textarea name='NewRL10'></textarea></td><td><textarea name='NewRL11'></textarea></td></tr>";
		}
		
		return $HTML;
	}
	
	// Returns HTML code displaying a table row containing the 10 receipt values set in the receipt section for this configuration.
	public function DisplayReceiptValues() {
		$HTML = "";
		
		if (isset($this->SelectedConfig)) { // A configuration is being displayed, show the saved data.
			$HTML .= "<tr><td>Values:</td><td><select name='RV2'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol2'])."</select></td><td><select name='RV3'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol3']);
			$HTML .= "</select></td><td><select name='RV4'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol4'])."</select></td><td><select name='RV5'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol5'])."</select></td>";
			$HTML .= "<td><select name='RV6'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol6'])."</select></td><td><select name='RV7'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol7'])."</select></td>";
			$HTML .= "<td><select name='RV8'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol8'])."</select></td><td><select name='RV9'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol9'])."</select></td>";
			$HTML .= "<td><select name='RV10'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol10'])."</select></td><td><select name='RV11'>".$this->AllRecTypes($this->Receipts['ReceiptValueCol11'])."</select></td></tr>";
		} else {
			$HTML .= "<tr><td>Values:</td><td><select name='NewRV2'>".$this->AllRecTypes()."</select></td><td><select name='NewRV3'>".$this->AllRecTypes()."</select></td><td><select name='NewRV4'>".$this->AllRecTypes()."</select></td>";
			$HTML .= "<td><select name='NewRV5'>".$this->AllRecTypes()."</select></td><td><select name='NewRV6'>".$this->AllRecTypes()."</select></td><td><select name='NewRV7'>".$this->AllRecTypes()."</select></td><td>";
			$HTML .= "<select name='NewRV8'>".$this->AllRecTypes()."</select></td><td><select name='NewRV9'>".$this->AllRecTypes()."</select></td><td><select name='NewRV10'>".$this->AllRecTypes()."</select></td>";
			$HTML .= "<td><select name='NewRV11'>".$this->AllRecTypes()."</select></td></tr>";
		}
		
		return $HTML;
	}
	
	// Returns HTML code displaying a table row containing the 10 disbursement labels set in the receipt section for this configuration.
	public function DisplayDisburseLabels() {
		$HTML = "";
		
		if (isset($this->SelectedConfig)) { // A configuration is being displayed, show the saved data.
			$HTML .= "<tr><td>Labels:</td><td><textarea name='DL2'/>".$this->Disbursements['DisburseLabelCol2']."</textarea></td><td><textarea name='DL3'/>".$this->Disbursements['DisburseLabelCol3']."</textarea></td><td><textarea name='DL4'/>".$this->Disbursements['DisburseLabelCol4']."</textarea></td>";
			$HTML .= "<td><textarea name='DL5'/>".$this->Disbursements['DisburseLabelCol5']."</textarea></td><td><textarea name='DL6'/>".$this->Disbursements['DisburseLabelCol6']."</textarea></td><td><textarea name='DL7'/>".$this->Disbursements['DisburseLabelCol7']."</textarea></td><td><textarea name='DL8'/>".$this->Disbursements['DisburseLabelCol8']."</textarea></td>";
			$HTML .= "<td><textarea name='DL9'/>".$this->Disbursements['DisburseLabelCol9']."</textarea></td><td><textarea name='DL10'/>".$this->Disbursements['DisburseLabelCol10']."</textarea></td><td><textarea name='DL11'/>".$this->Disbursements['DisburseLabelCol11']."</textarea></td></tr>";
		} else {
			$HTML .= "<tr><td>Labels:</td><td><textarea name='NewDL2'></textarea></td><td><textarea name='NewDL3'></textarea></td><td><textarea name='NewDL4'></textarea></td><td><textarea name='NewDL5'></textarea></td>";
			$HTML .= "<td><textarea name='NewDL6'></textarea></td><td><textarea name='NewDL7'></textarea></td><td><textarea name='NewDL8'></textarea></td><td><textarea name='NewDL9'></textarea></td>";
			$HTML .= "<td><textarea name='NewDL10'></textarea></td><td><textarea name='NewDL11'></textarea></td></tr>";
		}
		
		return $HTML;
	}
	
	// Returns HTML code displaying a table row containing the 10 disbursement values set in the receipt section for this configuration.
	public function DisplayDisburseValues() {
		$HTML = "";
		
		if (isset($this->SelectedConfig)) { // A configuration is being displayed, show the saved data.
			$HTML .= "<tr><td>Values:</td><td><select name='DV2'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol2'])."</select></td><td><select name='DV3'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol3']);
			$HTML .= "</select></td><td><select name='DV4'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol4'])."</select></td><td><select name='DV5'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol5'])."</select></td>";
			$HTML .= "<td><select name='DV6'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol6'])."</select></td><td><select name='DV7'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol7'])."</select></td>";
			$HTML .= "<td><select name='DV8'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol8'])."</select></td><td><select name='DV9'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol9'])."</select></td>";
			$HTML .= "<td><select name='DV10'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol10'])."</select></td><td><select name='DV11'>".$this->AllRecTypes($this->Disbursements['DisburseValueCol11'])."</select></td></tr>";
		} else {
			$HTML .= "<tr><td>Values:</td><td><select name='NewDV2'>".$this->AllRecTypes()."</select></td><td><select name='NewDV3'>".$this->AllRecTypes()."</select></td><td><select name='NewDV4'>".$this->AllRecTypes()."</select></td>";
			$HTML .= "<td><select name='NewDV5'>".$this->AllRecTypes()."</select></td><td><select name='NewDV6'>".$this->AllRecTypes()."</select></td><td><select name='NewDV7'>".$this->AllRecTypes()."</select></td><td>";
			$HTML .= "<select name='NewDV8'>".$this->AllRecTypes()."</select></td><td><select name='NewDV9'>".$this->AllRecTypes()."</select></td><td><select name='NewDV10'>".$this->AllRecTypes()."</select></td>";
			$HTML .= "<td><select name='NewDV11'>".$this->AllRecTypes()."</select></td></tr>";
		}
		
		return $HTML;
	}
	
	// Returns HTML code representing each receipt type found in the database as an option in a select list.
	//	$Target - If provided, causes the matching item to be selected by default.
	public function AllRecTypes($Target = null) {
		$HTML = "<option value='0'></option>";
		
		foreach($this->FeeTypes as $ID => $FeeName) {
			$HTML .= (isset($Target) && ($Target == $ID)) ? "<option value='$ID' selected>$FeeName</option>" : "<option value='$ID'>$FeeName</option>";
		}
		
		return $HTML;
	}
}
?>