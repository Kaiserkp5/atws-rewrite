<?php
/*
 Login form for the ATWS webpage.
*/

// Load page template
require_once('lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "ATWS", 'ContentBody' => __FILE__, 'FooterMedia' => "", 'Copyright' => "Copyright (c) Lieberman Technologies, LLC."));
    require "lib/layout.php";
    exit;
}
?>
	<section>
		<div id="wrapper" class="ResultsContainer">
			<div class="row" align="center">
				<div class="large-12 columns">
					<h3>ATWS</h3>
				</div>
			</div>
			<div class="row" align="center">
				<form id="loginform" action="index.php" method="post">
					<table class="sysmenu">
						<tr><th>Username:</th><td><input type="textbox" name="username"></input></td></tr>
						<tr><th>Password:</th><td><input type="password" name="password" class="smallpass"></input></td></tr>
						<tr><th></th><td><input type="submit" name="Submit" value="Login"></input></td></tr>
					</table>
				</form>
			</div>
		</div>
	</section>