<?php
require_once("../Includes/environment.php");
session_start(); // Continue old session
session_unset();
session_destroy();
header("Location: http://" . $config['webhost'] . "/dev/ATWS/login.php");
exit();
?>