<?php
/*
 * This page sets variables used in the page layout to display each webpage. A page template is built by each webpage before loading the layout.
*/
require_once("../../Includes/environment.php");
		
class PageTemplate {
	
    public $PageTitle;
    public $ContentBody;
	public $FooterMedia;
	public $Copyright;
	public $ScriptFile;
	
	function __construct($ParamArray = array()) {
		// Set each parameter in the array.
		foreach($ParamArray as $key => $value) {
			$this->$key = $value;
		}
	}

}

?>