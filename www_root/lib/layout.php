<?php
require_once('PageTemplate.php');
require_once("../../Includes/environment.php");
?>
<!DOCTYPE HTML>
<html>
	<header>
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
		<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Include stylish stylesheet. -->
		<link href="/dev/ATWS/lib/stylesheets/all.css" rel="stylesheet" type="text/css" />
		<link href="/dev/ATWS/lib/stylesheets/custom.css" rel="stylesheet" type="text/css" />
		
		<div class="contain-to-grid">
			<nav class="top-bar data-topbar">
				<section class="top-bar-section">
					<!-- Left Nav Section -->
					<ul class="left">
						<li><a class="navlink" href="http://localhost:82/dev/ATWS/index.php">Tax Warrants</a></li>
						<li><a class="navlink" href="http://localhost:82/dev/ATWS/WFDev/WorkforceMenu.php">Workforce Development</a></li>
						<li><a class="navlink" href="http://localhost:82/dev/ATWS/Reporting/ReportingMenu.php">Reporting</a></li>
						<li><a class="navlink" href="http://localhost:82/dev/ATWS/Receipting/ReceiptingMenu.php">Receipting System</a></li>
						<li><a class="navlink" href="http://localhost:82/dev/ATWS/Cashbook/CashbookController.php">Cashbook</a></li>
						<li><a class="navlink" href="http://localhost:82/dev/ATWS/Setup/Setup.php">Setup</a></li>
					</ul>
					<!-- Right Nav Section -->
					<ul class="right">
						<li><?php if (isset($_SESSION['LoggedIn']) && $_SESSION['LoggedIn'] === 'TRUE') echo "<a class='navlink' href='http://".$config['webhost']."/dev/ATWS/logout.php'>Welcome ".$_SESSION['user']."! Logout here.</a>";
								else echo "<a class='navlink' href='http://" . $config['webhost'] . "/dev/ATWS/login.php'>Login</a>"; ?></li>
					</ul>
				</section>
			</nav>
		</div>
	</header>

<?php if(isset($TPL->ContentBody)) { include $TPL->ContentBody; } ?>

<?php if(isset($TPL->ScriptFile)) { include $TPL->ScriptFile; } ?>

	<footer>
		<div class="row">
			<div class="large-8 columns">
				<p class="text-left"><?php if(isset($TPL->FooterMedia)) { echo $TPL->FooterMedia; } ?></p>
			</div>
			<div class="large-4 columns">
				<p class="text-right"><?php if(isset($TPL->Copyright)) { echo $TPL->Copyright; } ?></p>
			</div>
		</div>
	</footer>
</html>