<?php
/*
 This is the start page for the ATWS application. It links users to the warrant, receipting, and cashbook aspects of the application.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

// Create the page template.
require_once('../lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "Workforce Development", 'ContentBody' => __FILE__, 'FooterMedia' => "", 'Copyright' => "Copyright (c) Lieberman Technologies, LLC."));
    require "../lib/layout.php";
    exit;
}
?>
	<section>
		<div class="body-wrapper" id="wrapper">
			<div class="row" align="center">
				<div class="large-12 columns">
					<h3><?php echo $TPL->PageTitle; ?></h3>
				</div>
			</div>
			<div class="row" align="center">
				<div class="large-12 columns">
					<table class="sysmenu">
						<tr>
							<td colspan="2"><a href="">Nothing yet</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</section>
