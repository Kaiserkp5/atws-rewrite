<?php
/*
 This file serves as a standard MVC model for the receipt voiding pages.
 */
require_once "../../Includes/Queries.php";
 
class VoidReceiptModel {	
	/* ------- -------- ------- ------- -------- ------- ------- Input variables from user ------- -------- ------- ------- -------- ------- ------- */
	public $ReceiptNum;

	
	/* ------- -------- ------- ------- -------- ------- ------- Model functions ------- -------- ------- ------- -------- ------- ------- */
	// Constructor sets any value given.
	function __construct($ParamArray = array(), $FeeArray = array()) {
		/* Set each parameter in the array. */
		foreach($ParamArray as $key=>$value) {
			$this->$key = $value;
		}
	}
	
	// Returns "VALID" if this model is currently valid and an error message otherwise.
	public function IsValid() {
		// Make sure receipt number is numeric.
		
		return (is_numeric($this->ReceiptNum)) ? "VALID" : "Receipt Number must be numeric.";
	}
	
	// Voids the receipt number in this model.
	public function VoidReceipt() {
		$Query = new Queries();
		return $Query->VoidReceipt($this->ReceiptNum);
	}
	
	// Displays the given error message.
	public function DisplayMessage($Message) {
		echo "<script type='text/javascript'>alert('" . $Message . "')</script>";
	}
}
?>