<?php
/*
 This file serves as a standard MVC model for receipts.
 */
require_once "../../Includes/Queries.php";
 
class ReceiptModel {
	/* ------- -------- ------- ------- -------- ------- ------- Database query results ------- -------- ------- ------- -------- ------- ------- */	
	private $ReceiptTypes; // Array containing each receipt type. Key = TypeID, value = Receipt Title
	private $ReceiptRows; // Array containing each receipt type. Key = TypeID, value = Row input box appears on
	private $PaymentTypes; // Array of arrays. Key = PaymentTypeID, value = subarray( payment title, requires check )
	private $FeesList; // Array containing each fee appearing on the form. Key = Row on form, Value = Fee Title
	public $NextReceipt;

	
	/* ------- -------- ------- ------- -------- ------- ------- Input variables from user ------- -------- ------- ------- -------- ------- ------- */
	// The following vaiables are "receipt" variables - that is, they are attached to the receipt object itself.
	// The fees that follow will be saved as separate objects that reference the receipt they belong to.
	public $CauseNum;
	public $TWNum;
	public $ReceivedOf;
	public $ReceiptFor;
	public $PaymentType;
	public $CheckNum;
	public $Notes;
	public $TotalFees; // Sum of the charge on all fees attached to this receipt - will be stored on the receipt object.
	public $ReceiptType; // Determines which fees are available for this receipt and will appear in the following FeeArray.
	public $FeeArray; // This array has the form: key = OrderNum (1-9), value = FeeAmount. Stores the FeeAmount's entered by the user.
	public $NumReceiptsToPrint;

	
	/* ------- -------- ------- ------- -------- ------- ------- Model functions ------- -------- ------- ------- -------- ------- ------- */
	// Constructor sets query result parameters
	function __construct($ParamArray = array()) {
		/* Set each parameter in the array. */
		foreach($ParamArray as $key=>$value) {
			$this->$key = $value;
		}
		
		/* Populate the model parameters with receipt and fee data from the database. */
		// First access the Queries file to obtain an array that contains the results of all the queries we need to run.
		$Query = new Queries();
		$DBResults = array();
		$DBResults = $Query->GetReceiptModelInfo($this->ReceiptType);
		
		// Make sure the array isn't missing any elements.
		if ( !array_key_exists("PaymentTypesResult", $DBResults) || !array_key_exists("ReceiptTypesResult", $DBResults)
			 || !array_key_exists("FeesListResult", $DBResults) || !array_key_exists("NextReceiptResult", $DBResults) )
			die("Error 140 - Database results incomplete.");
		$this->ParsePaymentInfo($DBResults["PaymentTypesResult"]);
		$this->ParseReceiptInfo($DBResults["ReceiptTypesResult"]);
		$this->ParseFeeInfo($DBResults["FeesListResult"]);
		$this->ParseNextReceiptInfo($DBResults["NextReceiptResult"]);
		
		// If 'ReceiptFor' has no value, default it to the title of the selected receipt type
		if (empty($this->ReceiptFor) && $this->ReceiptType != "DEFAULT") $this->ReceiptFor = $this->ReceiptTypes[$this->ReceiptType];
		
		// Default number of receipts to print to 1
		$this->NumReceiptsToPrint = $this->NumReceiptsToPrint ?: 1;
	}
	
	// Parse payment info from a result set obtained by the GetReceiptModelInfo function. Called by the constructor.
	public function ParsePaymentInfo($PaymentResultSet) {
		$this->PaymentTypes = array(); // Initialize the array object.
		
		if (mysqli_num_rows($PaymentResultSet) > 0) {
			while ($row = $PaymentResultSet->fetch_assoc()) {
				$this->PaymentTypes[$row["Payment_Type"]] = array("PaymentTitle" => $row["Payment_Descr"], "RequiresCheck" => $row["AllowCheckNumberEntry"]);
			}
		} else { // Query found nothing.
			echo "Error 130 - No payment types were found.";
		}
	}
	
	// Parse receipt info from a result set obtained by the GetReceiptModelInfo function. Called by the constructor.
	public function ParseReceiptInfo($ReceiptResultSet) {
		$this->ReceiptTypes = array(); // Initialize the array object.
		
		if (mysqli_num_rows($ReceiptResultSet) > 0) {
			while ($row = $ReceiptResultSet->fetch_assoc()) {
				$this->ReceiptTypes[$row["ReceiptType"]] = $row["ReceiptTypeDesc"];
				$this->ReceiptRows[$row["ReceiptType"]] = $row["StubRow"];
			}
		} else { // Query found nothing.
			echo "Error 120 - No receipt types were found.";
		}
	}
	
	// Parse fee info from a result set obtained by the GetReceiptModelInfo function. Called by the constructor.
	public function ParseFeeInfo($FeeResultSet) {
		// Initialize the array object.
		$this->FeesList = array();
		
		if (mysqli_num_rows($FeeResultSet) > 0) {
			while ($row = $FeeResultSet->fetch_assoc()) {
				$this->FeesList[$row['stubRow']] = $row['stubRowDescription'];
			}
		} else { // Query for fees found nothing.
			echo "Error 100 - no fees could be found for this receipt type.";
		}
	}
	
	// Parse NextReceipt info from a result set obtained by the GetReceiptModelInfo function. Called by the constructor.
	public function ParseNextReceiptInfo($NextReceiptResultSet) {
		if (mysqli_num_rows($NextReceiptResultSet) > 0) {
			$this->NextReceipt = $NextReceiptResultSet->fetch_assoc(); // Get the row from this query (there's only one).
			$this->NextReceipt = ($this->NextReceipt['RecNum'] === NULL ? 1 : $this->NextReceipt['RecNum']); // Set NextReceipt to the actual number, not a DB result.
		} else { // Query for NextReceipt found nothing.
			echo "Error 150 - Next receipt number could not be determined.";
		}		
	}
	
	// Executes a query to update the database with a new receipt specified by the data currently stored in this model.
	public function CreateReceipt(){
		// Access the Queries file to handle receipt creation.
		$QueryInfo = new Queries();
		
		// Determine the service fee applied, if any.
		$ServiceFee = empty($this->FeeArray['1']) ? 0 : $this->FeeArray['1']; // TODO: If fees are made able to be re-ordered, this will have to change.
		
		$QueryInfo->CreateReceipt($this->CauseNum, $this->TWNum, $this->ReceivedOf, $this->ReceiptFor, $this->PaymentType, $this->CheckNum, $this->Notes,
									$this->TotalFees, $this->ReceiptType, $ServiceFee);
	}
	
	// Returns "VALID" if the data currently populating this model represents a valid receipt. Otherwise, returns an error message.
	public function IsValid(){
		// Make sure the user has entered all required fields.
		if (empty($this->PaymentType)) {return "Please enter a payment type.";}
		if ($this->PaymentTypes[$this->PaymentType]["RequiresCheck"] == 1 && $this->CheckNum <= 0) {return "Please enter a check number.";}
		if (empty($this->ReceivedOf)) {return "Please enter a value for Received Of";}
		if (empty($this->ReceiptFor)) {return "Please enter a reason for this receipt.";}
		
		// Check for valid TW/Cause numbers.
		if (empty($this->TWNum)) {return "Please enter a tax warrant number.";}
		if (empty($this->CauseNum)) {return "Please enter cause number.";}
		if (!is_numeric($this->TWNum)) {return "Tax Warrant Number must be numeric.";}
		if (!is_numeric($this->CauseNum)) {return "Cause Number must be numeric.";}
//TODO: check uniqueness of cause/tw numbers
		
		// Sum the fees entered by the user so we can compare it to the TotalFees field entered.
		$SumOfFees = 0;
		foreach ($this->FeeArray as $key=>$value)
			$SumOfFees += $value;
		if ($SumOfFees == 0) {return "Please enter at least one fee.";}
		if ($SumOfFees < 0) {return "Sum of fees was negative - please do not enter negative fee values.";}
		

		// Make sure a number of receipts to print is entered
		if (empty($this->NumReceiptsToPrint)) {return "Please enter a number of receipts to print.";}
		
		// Nothing failed, set the TotalFees value to the earlier calculated sum before returning success.
		$this->TotalFees = $SumOfFees;
		
		return "VALID";
	}
	
	// Displays the given error message.
	public function DisplayErrorMessage($Message) {
		echo "<script type='text/javascript'>alert('Failed - " . $Message . "')</script>";
	}
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// DisplayInput is used to return HTML that will display input boxes and titles for them. Users can customize the order in which these appear,
	// so which item is being displayed is determined by an order number that is passed in. The model constructor has fetched configuration data
	// for this receipt type already, telling us what needs to be displayed and in what order.
	public function DisplayInput($OrderNum) {
		$InputField = ""; // Initialize return string
		
		if ($OrderNum < 10) { // These are the 9 user-configured fields that appear first in the fees section of the entry form.
			// Determine whether this entry should have an active textbox.
			if ($this->ReceiptRows[$this->ReceiptType] == $OrderNum) { // Active textbox.
				$InputField = "<td>".$this->ReceiptTypes[$this->ReceiptType]."</td><td><input type='textbox' name='FeeEntry".$OrderNum."' id='FeeEntry".$OrderNum."' value='".$this->FeeArray[$OrderNum]."' /></td>";
			} else { // Inactive textbox.
				$InputField = "<td></td><td><input type='textbox' name='FeeEntry".$OrderNum."' readonly /></td>";
			}

		} else if ($OrderNum >= 10 || $OrderNum <= 13) { // Display one of the optional trusts (10 is A, 11 B, 12 C, 13 D)
			// Determine dropdown names and value.
			$SelectName = 'TrustAID';
			$InputName = 'TrustAAmount';
			if ($OrderNum == 11) {
				$SelectName = 'TrustBID';
				$InputName = 'TrustBAmount';
			} else if ($OrderNum == 12) {
				$SelectName = 'TrustCID';
				$InputName = 'TrustCAmount';
			} else if ($OrderNum == 13) {
				$SelectName = 'TrustDID';
				$InputName = 'TrustDAmount';
			}
			
			// Determine whether the dropdowns should be active.
			if ($this->ReceiptRows[$this->ReceiptType] == -1) { // Dropdowns active.
				$DropDown = "<td class='dropdown'><select name='$SelectName'><option value='0'></option><option value='".$this->ReceiptType."'>".$this->ReceiptTypes[$this->ReceiptType]."</option></select></td>";
				$EntryField = "<td class='dropdown'><input type='textbox' name='$InputName' id='$InputName' value='".$this->FeeArray[$OrderNum]."' /></td>";
				$InputField = $DropDown . $EntryField;
				
			} else { // Dropdowns inactive.
				$DropDown = "<td class='dropdown'><select disabled><option value='0'></option><option value='".$this->ReceiptType."'>".$this->ReceiptTypes[$this->ReceiptType]."</option></select></td>";
				$EntryField = "<td class='dropdown'><input type='textbox' value='' readonly /></td>";
				$InputField = $DropDown . $EntryField;
			}
			
		} else { // Bad order number given.
			$InputField = "<td>Error 110 - No entry with this order could be found.</td>";
		}
		
		// Return the input field that was built.
		return $InputField;
	}
	
	// Returns HTML to display all reecipt types found in the database when the constructor ran.
	public function DisplayInput_ReceiptType() {
		// HTML string containing the dropdown display to build and return. Start with beginning select.
		$Dropdown = "<select class='ReceiptTypeEntry' name='ReceiptType' onchange='this.form.submit()' value='" . $this->ReceiptType . "'>";
		
		// Add each option tag.
		foreach ($this->ReceiptTypes as $RID => $RTitle) {
			if ($this->ReceiptType == $RID) {$Dropdown .= "<option value='" . $RID . "' selected>" . $RTitle . "</option>";}
			else {$Dropdown .= "<option value='" . $RID . "'>" . $RTitle . "</option>";}
		}

		// Close the select tag.
		$Dropdown .= "</select>";
	
		return $Dropdown;
	}
	
	// Returns HTML to display all payment types found in the database when the constructor ran.
	public function DisplayInput_PaymentType() {
		// HTML string containing the dropdown display to build and return. Start with beginning select.
		$Dropdown = "<td class='dropdown'><select id='PaymentType' name='PaymentType' onchange='ShowHideCheckNum(this);' value='" . $this->PaymentType . "'><option value='0' name=''></option>";
		$CheckNumVisible = 0; // If no other option gets set "selected", the check number field will by default be hidden.
		
		// Add each option tag.
		foreach($this->PaymentTypes as $PID => $PArray) {
			if ($PID == $this->PaymentType){
				$Dropdown .= "<option value='" . $PID . "' selected>" . $PArray["PaymentTitle"] . "</option>";
				$CheckNumVisible = $PArray["RequiresCheck"]; // This will allow us to set the visibility to whatever this payment type requires.
			}
			else {$Dropdown .= "<option value='" . $PID . "'>" . $PArray["PaymentTitle"] . "</option>";}
		}
		
		// Close the select tag and create a new data cell for the input box.
		$Visibility = ($CheckNumVisible == 1) ? "visibile" : "hidden";
		$Dropdown .=  "</select></td><td><input type='textbox' placeholder='Check number...' id='CheckNum' name='CheckNum' value='" . $this->CheckNum . "' style='visibility:" . $Visibility . "' /></td>";
		
		return $Dropdown;
	}
	
	// The following input display functions all return simple HTML for a textbox with a specific name. These are common to
	// all receipt types and, unlike the fees handled by the DisplayInput function above, cannot be customized.
	public function DisplayInput_CauseNum() { return "<input type='textbox' name='CauseNum' value='" . $this->CauseNum . "' />"; }
	public function DisplayInput_TWNum() { return "<input type='textbox' name='TWNum' value='" . $this->TWNum . "' />"; }
	public function DisplayInput_ReceivedOf() { return "<input type='textbox' style='width:100%' name='ReceivedOf' value='" . $this->ReceivedOf . "' />"; }
	public function DisplayInput_ReceiptFor() { return "<input type='textbox' style='width:100%' name='ReceiptFor' value='" . $this->ReceiptFor . "' />"; }
	public function DisplayInput_Notes() { return "<textarea rows='2' name='Notes'>" . $this->Notes . "</textarea>"; }
	
	// Returns all the scripts generated by the model that should be at the bottom of the receipt entry screen.
	public function LoadScripts() {
		// First is the ShowHideCheckNum script.
		// This script causes the "check number" input box to only appear if the payment type is one that requires a check.
		$PaymentsList = ""; // First we build a list of all the payments that require a check number.
		foreach ($this->PaymentTypes as $key => $SubArray)
			if ($SubArray["RequiresCheck"] == 1)
				$PaymentsList .= ", " . $key; // Adding the comma before hand will give us a list with one extra comma at index 1, but no trailing comma at the end of the list.
		$PaymentsList .= "]";
		$PaymentsList[0] = "["; // Replaces the first character of the string (a comma, see comment above) with a bracket.
		$ShowHideCheckNum = "function ShowHideCheckNum(caller){var NeedCheckList = " . $PaymentsList . "; var flag = 0; for (i = 0; i < NeedCheckList.length; i++) "
							. "{if (caller.value == NeedCheckList[i]) {flag = 1;}} if (flag == 1){document.getElementById('CheckNum').style.visibility = 'visible';} "
							. "else {document.getElementById('CheckNum').style.visibility = 'hidden';}}";
		
		return "<script type='text/javascript'>" . $ShowHideCheckNum . "</script>";
	}
}
?>