<?php
/*
 This file provides users with a menu that allows them to select what they want to do - enter,
 print, or void receipts, or run a location deposit. Redirects as needed.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

require_once('../lib/PageTemplate.php');
// Load the page template (once).
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "Receipting System", 'ContentBody' => __FILE__, 'FooterMedia' => "", 'Copyright' => "Copyright (c) Lieberman Technologies, LLC."));
    require "../lib/layout.php";
	exit;
}
?>
	<section>
		<div class="body-wrapper" id="wrapper">
			<div class="row" align="center">
				<div class="large-12 columns">
					<h3><?php echo $TPL->PageTitle; ?></h3>
				</div>
			</div>
			<div class="row" align="center">
				<div class="large-12 columns">
					<table class="sysmenu">
						<tr><td><a href="EnterReceiptController.php">Enter Receipt</a></td></tr>
						<tr><td><a href="">Batch Print Receipts</a></td></tr>
						<tr><td><a href="">Reprint Receipt</a></td></tr>
						<tr><td><a href="VoidReceipt.php">Void Existing Receipt</a></td></tr>
						<tr><td><a href="">Run Location Deposit</a></td></tr>
					</table>
				</div>
			</div>
		</div>
	</section>
