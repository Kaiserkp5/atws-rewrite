<?php
/*
 Controller for the receipt entry system.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

// Create the model. If no button press was posted, user just navigated to the page and no further work needs to be done.
// Otherwise, the form was submitted and the controller needs to populate the model with values we just received.
require_once "/ReceiptModel.php";
if (isset($_POST['command'])) { // Update the model with everything the user has submitted.
	$ParamArray = array();
	$FeeArray = array();
	$ParamArray['ReceiptType'] = $_POST['ReceiptType'];
	$ParamArray['CauseNum'] = $_POST['CauseNum'];
	$ParamArray['TWNum'] = $_POST['TWNum'];
	$ParamArray['ReceivedOf'] = $_POST['ReceivedOf'];
	$ParamArray['ReceiptFor'] = $_POST['ReceiptFor'];
	$ParamArray['PaymentType'] = $_POST['PaymentType'];
	$ParamArray['CheckNum'] = $_POST['CheckNum'];
	$ParamArray['Notes'] = $_POST['Notes'];
	$ParamArray['TotalFees'] = $_POST['TotalFees'];
	$FeeArray['1'] = $_POST['FeeEntry1'];
	$FeeArray['2'] = $_POST['FeeEntry2'];
	$FeeArray['3'] = $_POST['FeeEntry3'];
	$FeeArray['4'] = $_POST['FeeEntry4'];
	$FeeArray['5'] = $_POST['FeeEntry5'];
	$FeeArray['6'] = $_POST['FeeEntry6'];
	$FeeArray['7'] = $_POST['FeeEntry7'];
	$FeeArray['8'] = $_POST['FeeEntry8'];
	$FeeArray['9'] = $_POST['FeeEntry9'];
	$FeeArray['10'] = $_POST['TrustAAmount'];
	$FeeArray['11'] = $_POST['TrustBAmount'];
	$FeeArray['12'] = $_POST['TrustCAmount'];
	$FeeArray['13'] = $_POST['TrustDAmount'];
	$ParamArray['FeeArray'] = $FeeArray;
	
	$model = new ReceiptModel($ParamArray);
	
	// Check to see what button was pressed and respond accordingly.
	if ($_POST['command'] == "Create and Print") { // Save the receipt in the database, open the print dialogue box.
		// Validate input, create receipt or display error.
		$ValidationMessage = $model->IsValid();
		if ($ValidationMessage == "VALID"){
			$model->CreateReceipt();
			// print
		} else {
			$model->DisplayErrorMessage($ValidationMessage);
		}
		
	} else if ($_POST['command'] == "Back") { // Redirect user to the menu page.
		header("Location: http://" . $config['webhost'] . "/dev/ATWS/Receipting/ReceiptingMenu.php");
		exit();
	} //else if ($_POST['command'] == "Refresh") { } Nothing more needs to be done when the refresh button is clicked.
	
} else if (isset($_POST['ReceiptType'])) { // No button set, but there is a receipt type. This occurs when a user changes the receipt type dropdown.
	$ParamArray = array();
	$ParamArray['ReceiptType'] = $_POST['ReceiptType'];
	// The fee info is being lost when they change receipt types, but we can save any receipt info if it was set.
	if (isset($_POST['CauseNum'])) {$ParamArray['CauseNum'] = $_POST['CauseNum'];}
	if (isset($_POST['TWNum'])) {$ParamArray['TWNum'] = $_POST['TWNum'];}
	if (isset($_POST['ReceivedOf'])) {$ParamArray['ReceivedOf'] = $_POST['ReceivedOf'];}
	if (isset($_POST['ReceiptFor'])) {$ParamArray['ReceiptFor'] = $_POST['ReceiptFor'];}
	if (isset($_POST['PaymentType'])) {$ParamArray['PaymentType'] = $_POST['PaymentType'];}
	if (isset($_POST['Notes'])) {$ParamArray['Notes'] = $_POST['Notes'];}
	
	$model = new ReceiptModel($ParamArray);
	
} else { // Nothing is set, user just navigated to the page. Display default values.
	$ParamArray = array();
	$ParamArray['ReceiptType'] = "DEFAULT";
	$model = new ReceiptModel($ParamArray);
}

// Create the page template.
require_once('../lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "Enter Receipt", 'ContentBody' => REALPATH(DIRNAME(__FILE__)) . '/EnterReceipt.html', 'FooterMedia' => "", 'Copyright' => "Copyright (c) Lieberman Technologies, LLC."));
    require "../lib/layout.php";
	exit;
}

?>