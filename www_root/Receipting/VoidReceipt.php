<?php
/*
 Controller for the receipt entry system.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

// Get user input and react accordingly.
require_once("VoidReceiptModel.php");
if (isset($_POST['command'])) {
	
	// Check to see what button was pressed and respond accordingly.
	if ($_POST['command'] == "Void") {
		// Void the receipt if the given receipt number exists. Return an error otherwise.
		$ParamArray = array();
		$ParamArray['ReceiptNum'] = $_POST['ReceiptNum'];
		$model = new VoidReceiptModel($ParamArray);
		
		$ValMsg = $model->IsValid(); // Returns "VALID" or an error message.
		
		if ($ValMsg === "VALID") { // First check to see that the input given is valid.
			// Next check to see that the database could find a receipt with this number.
			$ValMsg = $model->VoidReceipt(); // Will return 'success' or 'could not find receipt'.
			$model->DisplayMessage($ValMsg);
		} else { // Input was bad.
			$model->DisplayMessage($ValMsg);
		}
	}
	
} else { // User just navigated to the page, load model with default parameters.
	$ParamArray = array();
	$model = new VoidReceiptModel($ParamArray);
}

// Create the page template.
require_once('../lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "Void Receipt", 'ContentBody' => REALPATH(DIRNAME(__FILE__)) . '/VoidReceipt.html', 'FooterMedia' => "", 'Copyright' => "Copyright (c) Lieberman Technologies, LLC."));
    require "../lib/layout.php";
	exit;
}
?>