<?php
/*
 This file serves as a standard MVC model for disbursements.
 */
require_once "../../Includes/Queries.php";
 
class DisburseModel {	
	/* ------- -------- ------- ------- -------- ------- ------- Model Parameters ------- -------- ------- ------- -------- ------- ------- */
	/* These fields are passed in by the controller. */
	public $Menu_BookSelected; 	// These two $Menu fields (book and month) represent an entry in the cashbook that the user is viewing. The payment being
	public $Menu_TargetMonth;  	// disbursed (identified by the ID below) is contained within the cashbook entry identified by this book-month pair.
	public $LineNum;		// The line number of the payment being disbursed.
	public $CBcol;			// Receipt column that the payment was under. Corresponding database column is 'Col_R_' . ($CBcol - 1)
	public $DisburseDate;	// Date of disbursement.
	
	/* These are the fields entered by the user. */
	public $PayTo; 				// Value from the 'Pay to the order of' textbox.
	public $PayFor; 			// Value from the 'For' textbox.
	public $DisburseTo; 		// The category this payment will be disbursed to. This is a FeeType that appears in the Disbursement side fo the selected cashbook.
	public $DisburseCauseNum; 	// A new cause number may be entered to charge against. The CauseNum field above is the number of the receipt we are disbursing money from.
	public $DisburseTaxWarrant;	// A new tax warrant number may be entered to charge against. The TaxWarrantNum field above is the number of the receipt we are disbursing money from.
	public $DisburseAmount;		// The amount of money to be disbursed.
	
	/* These fields are fetched from the database on model creation. */
	public $CashReceived;	// Total amount received that can be disbursed.
	public $CashDisbursed;	// Total amount that has already been disbursed.
	public $CBColTitle;		// Title of the fee type that appears in the receipt column that is being disbursed.
	public $ValidDisbCols;	// Array contains the valid columns to disburse to. $key[$val] => Column Number[Column Title]
	public $NextCheck;		// This check number will be displayed on the page. This variable will not actually be used when printing - the check number will be fetched again just before printing.
	public $receiptType;	// Type of receipt being disbursed. This will not be displayed to the user. Disbursements are recorded with the same receipt type as that of the payment being disbursed.
	
	// Constructor sets query result parameters.
	function __construct($ParamArray = array()) {
		// Set each parameter in the array.
		foreach($ParamArray as $key=>$value) {
			$this->$key = $value;
		}
		
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$DisbResult = GetDisbursementInfo($this->LineNum, 'Col_R_' . ($this->CBcol - 1)); // Query finds information about the payment being disbursed.
		$DisbColsResult = $Query->GetBookConfig($this->Menu_BookSelected); // Query for config data for this book. We need to know what columns are valid to disburse to for this book.
		$NextCheckResult = $Query->NextCheckNum(); // Gets the next check number to be used.
		
		// Now parse the data retrieved into arrays in a format usable by the model.
		$this->ParseDisbInfo($DisbResult);
		$this->ParseConfInfo($DisbColsResult);
		$this->ParseNextCheckInfo($NextCheckResult);
	}
	
	// Parses info found in the database regarding valid disbursement options into a format usable by this model.
	public function ParseDisbInfo($DisburseResult) {
		if (mysqli_num_rows($DisburseResult) > 0) {
			$row = $DisburseResult->fetch_assoc();
			$this->CashReceived = $row['CashReceived'];
			$this->CashDisbursed = $row['TotalCashDisbursed'];
			$this->receiptType = $row['receiptType'];
			
		} else { // Query found nothing.
			echo "Error 310 - Next check number could not be determined.";
		}
	}
	
	// Parses info found in the database regarding valid disbursement options into a format usable by this model.
	public function ParseConfInfo($DisbColsResult) {
		$this->ValidDisbCols = array(); // Initialize array.
		
		if (mysqli_num_rows($DisbColsResult) > 0) {
			// Each row will contain a number (1-20) and a header.
			// We want all disbursement column headers (11-20) and the receipt column header that the money currently being disbursed was under.
			while ($row = $DisbColsResult->fetch_assoc()) {
				if ($row['columnNumber'] = $this->CBcol - 1) { // This is the receipt column we wanted.
					$this->CBColTitle = $row['colHeader'];
				}
				
				if ($row['columnNumber'] > 10) { // Record the disbursement column data.
					$this->ValidDisbCols[$row['columnNumber']] = $row['colHeader'];
				}
			}
			
		} else { // Query found nothing.
			echo "Error 320 - Could not load disbursement data for book.";
		}
	}
	
	// Parses the next check number found in the database.
	public function ParseNextCheckInfo($NextCheckResultSet) {
		if (mysqli_num_rows($NextCheckResultSet) > 0) {
			$row = $NextCheckResultSet->fetch_assoc(); // Get the row from this query (there's only one).
			$this->NextCheck = ($row['CheckNum'] === NULL ? 1 : $row['CheckNum']);
		} else { // Query for NextCheck found nothing.
			echo "Error 340 - Next check number could not be determined.";
		}
	}
	
	// Connects to the database and enters a disbursement as defined by the user.
	public function DisbursePayment() {
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$Query->DisbursePayment($this->LineNum, $this->DisburseTo, $this->DisburseAmount, $this->DisburseDate, $this->PayTo, $this->PayFor, $this->DisburseCauseNum, $this->DisburseTaxWarrant, $this->receiptType);
	}
	
	// Logs the given error text in the given error log file. Creates the file if it does not exist.
	public function LogDBError($ErrorMessage, $ErrorFile) {
		try {
			file_put_contents($ErrorFile, $ErrorMessage . "\r\n", FILE_APPEND | LOCK_EX);
		}
		catch (Exception $e) {
			// If we fail to write to the error log there's little we can do except exit as gracefully as possible.
		}
	}
	
	// Returns "VALID" if the current configuration of this model represents a valid disbursement ready to be inserted into the database and an error message otherwise.
	public function IsValid() {
		// Return an error if any required fields are empty.
		if (empty($this->LineNum)) { return "Error 350 - LineNum could not be found."; }
		if (empty($this->DisburseTo)) { return "Error 360 - Could not load Disburse To column."; }
		if (empty($this->DisburseAmount)) { return "Please enter an amount to disburse."; }
		if (empty($this->DisburseDate)) { return "Error 370 - Could not load Disburse Date."; }
		if (empty($this->NextCheck)) { return "Error 380 - Could not load next check number."; }
		if (empty($this->PayTo)) { return "Pay to the order of field cannot be empty." ;}
		if (empty($this->PayFor)) { return "For field cannot be empty."; }
		
		// User cannot disburse more money than exists, or a non-numeric amount.
		if (!is_numeric($this->DisburseAmount)) { return "Disburse amount must be numeric."; }
		if ($this->DisburseAmount > ($this->FullAmount - $this->PaidSoFar)) { return "The maximum amount that can be disbursed on this payment is $" . ($this->FullAmount - $this->PaidSoFar); }
		
		// Tax warrant and cause number fields???
		
		
		// If nothing was wrong...
		return "VALID";
	}
	
	// Displays the given error message.
	public function DisplayErrorMessage($Message) {
		echo "<script type='text/javascript'>alert('Failed - " . $Message . "')</script>";
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// This function displays the reminder text at the top of the table, giving the user a few details about the payment they chose to disburse.
	public function DisplayInput_InfoText() {
		echo "<td class='InfoText' colspan='4'>Disbursing up to $" . number_format($this->CashReceived - $this->CashDisbursed, 2, '.', ',') . " from cashbook column '" . $this->CBColTitle . "'.</td>";
	}
	
	// This function displays a dropdown that contains each disbursement column that appears in the currently selected cashbook.
	public function DisplayInput_DisburseTo() {
		// Begin the select tag (and other formatting tags for this row).
		echo "<th class='DB'>Disburse to</th><td colspan='2'><select class='DisburseTo' name='DisburseTo' value='" . $this->DisburseTo . "'>";
		
		// Add each option.
		foreach ($this->ValidDisbCols as $ID => $ValArray) {
			if (isset($this->DisburseTo) && ($ValArray['Value'] == $this->DisburseTo)) // If a DisburseTo category is already selected (perhaps the form was refreshed), keep it selected.
				echo "<option value='" . $ValArray['Value'] . "' selected>" . $ValArray['Label'] . "</option>";
			else
				echo "<option value='" . $ValArray['Value'] . "'>" . $ValArray['Label'] . "</option>";
		}
		
		// Close off the select tag and formatting tags.
		echo "</select></td><td></td>";
	}
	
	// This function displays a textbox who's value is tied to the DisburseAmount parameter in this model.
	public function DisplayInput_DisburseAmount() {
		echo "<th class='DB'>Amount</th><td><input type='textbox' name='DisburseAmount' value='" . $this->DisburseAmount . "' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the DisburseDate parameter in this model.
	public function DisplayInput_DisburseDate() {
		echo "<th class='DB'>Date</th><td><input type='textbox' value='" . $this->DisburseDate . "' readonly /></td>";
	}
	
	// This function displays a textbox who's value is tied to the cause number parameter in this model.
	public function DisplayInput_CauseNum() {
		echo "<th class='DB'>Cause Number</th><td><input type='textbox' name='CauseNum' value='" . $this->DisburseCauseNum . "' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the tax warrant number parameter in this model.
	public function DisplayInput_TWNum() {
		echo "<th class='DB'>Tax Warrant #</th><td><input type='textbox' name='TaxWarrantNum' value='" . $this->DisburseTaxWarrant . "' /></td>";
	}
	
	// This function displays a cell containing the next check number to be printed.
	public function DisplayInput_NextCheck() {
		echo "<th class='DB'>Next Check # " . $this->NextCheck . "</th><td><input type='submit' name='command' value='Refresh Check' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the PayTo parameter in this model.
	public function DisplayInput_PayTo() {
		echo "<th class='DB'>Pay to the order of</th><td colspan='3'><input type='textbox' name='PayTo' style='width:100%' value='" . $this->PayTo . "' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the PayFor parameter in this model.
	public function DisplayInput_PayFor() {
		echo "<th class='DB'>For</th><td colspan='3'><input type='textbox' style='width:100%' name='PayFor' value='" . $this->PayFor . "' /></td>";
	}
	
	// Displays two hidden inputs that remember the book and month selected by the user, and one that stores the column number that a payment appeared in when the user disburses it.
	public function DisplayInput_HiddenData() {
		echo "<input type='hidden' id='MenuBookTitle' name='MenuBookTitle' value='" . $this->Menu_BookSelected . "' />" .
			 "<input type='hidden' id='TargetMonth' name='TargetMonth' value='" . $this->Menu_TargetMonth . "' />" .
			 "<input type='hidden' id='RecCol' name='RecCol' value='" . $this->CBcol . "' />" . // When a user disburses a payment, we need the column number it appears in to display some help text on the next page.
			 "<input type='hidden' id='LineNum' name='LineNum' value='" . $this->LineNum . "' />";
	}
}
?>