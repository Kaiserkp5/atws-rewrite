<?php
/*
 This file serves as a standard MVC model for the disbursement review page.
 */
require_once "../../Includes/Queries.php";
 
class DisburseReviewModel {
	/* ------- -------- ------- ------- -------- ------- ------- Database query results ------- -------- ------- ------- -------- ------- ------- */
	private $FeeInfo; // Array of arrays. DisburseID => [Fee Info].
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input variables from user ------- -------- ------- ------- -------- ------- ------- */
	public $Menu_BookSelected; 	// These two $Menu fields (book and month) represent an entry in the cashbook that the user is viewing. The payment being
	public $Menu_TargetMonth;  	// disbursed (identified by the ID below) is contained within the cashbook entry identified by this book-month pair.
	public $FeeID; // Type of fee corresponding to the selected disbursement column.
	public $ReceiptID; // Receipt that the selected disbursements were made against.
	public $ReviewCol; // The label for the disbursement column that this fee type was assigned to. Used in the header at the top of this page.
	
	// Constructor sets query result parameters.
	function __construct($ParamArray = array()) {
		// Set each parameter in the array.
		foreach($ParamArray as $key=>$value) {
			$this->$key = $value;
		}
		
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$FeesResult = $Query->GetReceiptFees($this->ReceiptID, $this->FeeID); // Query for the info about the selected payment to be disbursed.
		
		// Now parse the data retrieved into arrays in a format usable by the model.
		$this->ParseFeeInfo($FeesResult);
	}
	
	// Parses info found in the database regarding the selected fees.
	public function ParseFeeInfo($FeesResult) {
		$this->FeeInfo = array(); // Initialize the array.
		
		if (mysqli_num_rows($FeesResult) > 0) {
			// Create a subarray for each fee found.
			while ($row = $FeesResult->fetch_assoc()) {
				$this->FeeInfo[$row['DisbursementUID']] = array('ReceiptID' => $row['ReceiptUID'], 'FeeTypeID' => $row['FeeID'], 'PaymentID' => $row['PaymentUID'], 'DisburseDate' => $row['DateDisbursed'],
								'Amount' => $row['Amount'], 'CauseNum' => $row['CauseNum'], 'TaxWarrant' => $row['TaxWarrantNum'], 'TotalPayment' => $row['TotalPayment'], 'CheckNum' => $row['CheckNum']);
			}
		} else { // Query found nothing.
			echo "Error 400 - Could not load fee data.";
		}
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// Displays the header at the top of the page.
	public function DisplayHeader() {
		echo "<h4>Disbursements made in '$this->ReviewCol' column on receipt number $this->ReceiptID</h4>";
	}
	
	// This function displays a dropdown that contains each disbursement column that appears in the currently selected cashbook.
	public function DisplayFeeTable() {
		// Display a header row.
		echo "<tr><th>Tax Warrant</th><th>Cause Number</th><th>Check Number</th><th>Date Disbursed</th><th>Amount Disbursed</th><th>Payment Total</th></tr>";
		
		// Add a row to the table for each fee.
		foreach ($this->FeeInfo as $ID => $ValArray) {
			echo "<tr><td>" . $ValArray['TaxWarrant'] . "</td><td>" . $ValArray['CauseNum'] . "</td><td>" . $ValArray['CheckNum'] . "</td><td>"
						. $ValArray['DisburseDate'] . "</td><td>" . $ValArray['Amount'] . "</td><td>" . $ValArray['TotalPayment'] . "</td></tr>";
		}
	}
	
	// Displays two hidden inputs that remember the book and month selected by the user, and one that stores the column number that a payment appeared in when the user disburses it.
	public function DisplayHiddenData() {
		echo "<input type='hidden' id='MenuBookTitle' name='MenuBookTitle' value='" . $this->Menu_BookSelected . "' />" .
			 "<input type='hidden' id='TargetMonth' name='TargetMonth' value='" . $this->Menu_TargetMonth . "' />";
	}
}
?>