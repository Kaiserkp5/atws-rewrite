<?php
/*
	ALTERNATE VIEWS FOR BOOKS HAVE BEEN SCRAPPED

 This file serves as a standard MVC model for cashbooks. Displays data in "Compact" view. All data is organized around receipts. Payments and consolidated Disbursements appear on the same line.
 */
require_once "../../Includes/Queries.php";
require_once "/CashbookModel.php";

class CashbookCompactModel extends CashbookModel {
	// Fetches database information needed for this model and returns the query results.
	protected function FetchDBInfo() {
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$BookResult_Receipt = $Query->GetReceiptQueryForBook($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Disburse = $Query->GetDisburseQueryForBook($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Payment = $Query->GetPaymentQueryForBook($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Config = $Query->GetCashbookConfiguration($this->Menu_BookSelected);
		$BroughtForwardResult = $Query->GetAmountBroughtForward($this->Menu_BookSelected, $this->Menu_TargetMonth);
		
		return array('BookResult_Config' => $BookResult_Config, 'BookResult_Receipt' => $BookResult_Receipt, 'BookResult_Disburse' => $BookResult_Disburse, 'BookResult_Payment' => $BookResult_Payment, 'BroughtForwardResult' => $BroughtForwardResult);
	}
	
	// Parses data retrieved from the database and stores it in the DisburseData array. This is each disbursed payment found for the month.
	protected function ParseDisbursementInfo($BookResult_Disburse) {
		$this->DisburseData = array(); // Initialize array.
		
		if (mysqli_num_rows($BookResult_Disburse) > 0) {
			// Create an array for each receipt.
			while ($row = $BookResult_Disburse->fetch_assoc()) {
				if (array_key_exists($row['ReceiptID'], $this->DisburseData)) { // If an entry for this receipt exists, add this FeeID and total to the inner array.
					$this->DisburseData[$row['ReceiptID']][$row['FeeID']] = $row['TotalDisb'];
				} else { // No inner array found, create an inner array for this receipt ID.
					$this->DisburseData[$row['ReceiptID']] = array($row['FeeID'] => $row['TotalDisb']);
				}
			}
		} else { // Query found nothing.
			// This error does not need to be echoed in production, as 0 records may simply indicate that nothing has been entered yet this month.
			//echo "Error 223 - Could not load disbursement data for book.";
		}
	}
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// Displays the view tab at the top of the page, allowing users to switch between available views.
	public function DisplayViewMode() {
		echo "<table class='ViewPanel'><tr><th>View: </th>" .
			 "<td><button class='ViewInactive' name='View' onclick='SwitchView()' value='Side'>Side by Side</button></td>" .
			 "<td>Compact</td>" .
			 "<td><button class='ViewInactive' name='View' onclick='SwitchView()' value='Singular'>Singular</button></td></tr></table>";
	}
	
	// Displays the first row of the table, separating the table into 3 sections.
	public function DisplaySectionHeader() {
		echo "<th class='TX' colspan='5'><th class='RT' colspan='11'>RECEIPTS</th><td class='smallgap'></td><th class='DB' colspan='11'>DISBURSEMENTS</th>";
	}
	
	// Displays the receipt data column headers.
	public function DisplayReceiptHeader() {
		echo "<tr><td class='TX'>DATE </br> (MM-DD)</td><td class='TX'>RECEIPT NUMBER</td> <td class='TX'>CAUSE NUMBER</td> "
			 . "<td class='OWA'>FROM WHOM RECEIVED</td> <td class='OWA'>ON WHAT ACCOUNT</td>";
	}
	
	// Displays the receipt info for the selected month and cashbook. Orders data to match the column names as configured by the user.
	public function DisplayInput_ReceiptDisbursementData () {
		
		// Display each receipt found and stored in the receipt array.
		foreach ($this->ReceiptData as $RID => $Receipt) {
			
			// Taxpayer info section.
			echo "<tr><td class='TX'>" . substr($Receipt["DateReceived"], 5, 5) . "</td><td class='TX'>" . $RID . "</td><td class='TX'>" .
					 $Receipt["CauseNum"] . "</td><td class='OWA'><div class='scrollable'>". $Receipt["ReceivedOf"] . "</div></td><td class='OWA'><div class='scrollable'>" .
					 $Receipt["ReceivedFor"] . "</div></td>";
					 
			// Receipt section. Show payments made on a receipt (row) for each receipt type (column), where columns are configured by the user.
			 // Receipt column 1.
			echo "<td class='RT'>" . $Receipt['TotalCashReceived'] . "</td>";
			 // Receipt columns 2-11.
			for ($i = 2; $i <= 11; $i++) {
				$FeeForThisCol = $this->BookConfig["ReceiptValueCol" . $i]; // This is the fee type that should appear in the current column.
				//$this->PaymentData[$RID]; 				 <-- This is the array of all fees attached to the receipt with ID $RID.
				$ReceiptContainsFeeType = array_key_exists($FeeForThisCol, $this->PaymentData[$RID]); // TRUE if this receipt had a payment with the specified fee type.
				
				if ($ReceiptContainsFeeType) {
					// First we create a copy the entry in the array to make the following statements less verbose.
					$PaymentArray = $this->PaymentData[$RID][$FeeForThisCol];
					
					// Receipt contains a payment with the specified fee type, now determine whether it has been fully disbursed.
					if ($PaymentArray['PaidSoFar'] >= $PaymentArray['Amount']) // Payment has been disbursed, just display the amount (now 0).
						echo "<td class='RT'>" . $PaymentArray['Amount'] . "</td>";
					else if ($PaymentArray['PaidSoFar'] > 0) // Payment has been disbursed in part, display the amount remaining in yellow.
						echo "<td class='RT'><button class='cashpart' name='PayID' onclick='BindCommandAndSubmit(" . $i . ")' value='" . $PaymentArray['PaymentUID'] . "'>" . $PaymentArray['Amount'] . "</button></td>";
					else // Payment has not yet been disbursed at all. Display the full amount of the payment in green.
						echo "<td class='RT'><button class='cashbook' name='PayID' onclick='BindCommandAndSubmit(" . $i . ")' value='" . $PaymentArray['PaymentUID'] . "'>" . $PaymentArray['Amount'] . "</button></td>";
				} else // This receipt had no payment with the specified fee type.
					echo "<td class='RT'>0</td>";
			}
			
			// Add a small divider between the Receipt and Disbursement sections.
			echo "<td class='smallgap'></td>";
			
			// Disbursement section. Recall that each cashbook cell is defined by a receipt type (row) and fee type (column). The cells here in the disbursement section need to show the sum of all
			// disbursements paid on the payment designated by this row/column (receipt/fee type), if such a payment exists.
			$ReceiptDisbursed = array_key_exists($RID, $this->DisburseData); // TRUE if there was a disbursement for this receipt.
			 
			 // First column, total disbursement on this receipt.
			if ($ReceiptDisbursed) {
				$Total = 0;
				foreach ($this->DisburseData[$RID] as $FeeID => $Amount)
					$Total += $Amount;
				echo "<td class='DB'>$Total</td>";
			} else // Nothing to display for this cell.
				echo "<td class='DB'>--</td>";
			
			 // Columns 2-11, as configured by user.
			for ($i = 2; $i <= 11; $i++) {
				//$this->BookConfig["DisburseValueCol" . $i]; <-- This is the fee type that should appear in the current column.
				//$this->DisburseData[$RID]; 				  <-- This is the array of all [FeeID => Sum(Amount)] values for the given receipt type.
				
				if ($ReceiptDisbursed) {
					$ReceiptContainsDisb = array_key_exists($this->BookConfig["DisburseValueCol" . $i], $this->DisburseData[$RID]); // TRUE if this receipt had a disbursement for a payment with this fee type.
					if ($ReceiptContainsDisb)
						echo "<td class='DB'><button class='Disbbook' name='DisRec' onclick=\"BindDisbAndSubmit(" . $this->BookConfig["DisburseValueCol" . $i] . ", '" . ($this->BookConfig["DisburseLabelCol" . $i]) . "')\" value='$RID'>" . $this->DisburseData[$RID][$this->BookConfig["DisburseValueCol" . $i]] . "</button></td>";
					else // Nothing to display for this cell.
						echo "<td class='DB'>--</td>";
				} else { // Nothing to display for this cell.
					echo "<td class='DB'>--</td>";
				}
			}
			// End the row.
			echo "</tr>";
		}
	}
}
?>