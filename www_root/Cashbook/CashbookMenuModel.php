<?php
/*
 This file serves as a standard MVC model for cashbooks.
 */
require_once "../../Includes/Queries.php";
 
class CashbookMenuModel {
	/* ------- -------- ------- ------- -------- ------- ------- Database query results ------- -------- ------- ------- -------- ------- ------- */
	public $MenuResult_BookTitles;
    public $MenuResult_DateQuery;
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input variables from user ------- -------- ------- ------- -------- ------- ------- */
	public $Menu_BookSelected;
	public $Menu_TargetMonth;
	
	
	// Constructor sets query result parameters
	function __construct($ParamArray = array()) {
		// Set each parameter in the array.
		foreach($ParamArray as $key => $value) {
			$this->$key = $value;
		}
		
		// Create a database connection object and get data needed for the page.
		$Query = new Queries();
		$this->MenuResult_BookTitles = $Query->GetBookTitles(); // Query for books in the database.
		$this->MenuResult_DateQuery = $Query->GetBookEntriesByTitle($this->Menu_BookSelected); // Query for months in the specified book that have data.
		
	}
	
	public function AddNew() {
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$Query->AddMonthToBook($this->Menu_BookSelected)
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// Displays the first dropdown box on the checkbook menu, allowing users to choose the book they want to view.
	public function DisplayInput_MenuBookDropdown() {
		echo "<select style='width:25%' onchange='BindCommAndSubmit()' name='MenuBookTitle'><option disabled selected> -- select a book -- </option>"; // Build a dropdown object.
		
		// Add option tags
		if (mysqli_num_rows($this->MenuResult_BookTitles) > 0) {
			while ($row = $this->MenuResult_BookTitles->fetch_assoc()) {
				// IF clause checks to see if the current option to be placed in the dropdown is the currently selected book. If so, make it the item currently selected in the dropdown.
				if ($row["BookTitle"] == $this->Menu_BookSelected){ echo "<option value='" . $row["BookTitle"] . "' selected>" . $row["BookTitle"] . "</option>";}
				else { echo "<option value='" . $row["BookTitle"] . "'>" . $row["BookTitle"] . "</option>";}
			}
			
		} else { // Query for books found nothing.
			echo "No books were found.";
		}
		echo "</select>";
	}
	
	// Displays the second dropdown on the cashbook menu, allowing users to select the month they want to see in the selected book.
	public function DisplayInput_MenuDateDropdown() {
		// Display a table showing the months found by the query.
		if (mysqli_num_rows($this->MenuResult_DateQuery) > 0) {
			echo "<select style='width:35%' name='TargetMonth'>";
			while ($row = $this->MenuResult_DateQuery->fetch_assoc()) {
				echo "<option value='" . $row["ReportingYear"] . "-" . $row["ReportingMonth"] . "-01'>" . DateTime::createFromFormat('!m',$row["ReportingMonth"])->format('F') . " " . $row["ReportingYear"] . " " . ($row["Closed"] ? "(closed)" : "(open)") . "</option>";
			}
			echo "</select> </br> <input type='submit' name='command' value='New' /> <input type='submit' name='command' value='Submit' />";
		} else { // Query for months found nothing.
			echo "Please select a book.";
		}
	}

}
?>