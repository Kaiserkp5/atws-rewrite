<?php
/*
	ALTERNATE VIEWS FOR BOOKS HAVE BEEN SCRAPPED

 This file serves as a standard MVC model for cashbooks. Displays data in "Singular" view. Receipt and Disbursement data are both
 display in the same manner as in the "Side by Side" view, but are displayed on separate pages that the user can swap between.
 */
require_once "../../Includes/Queries.php";
require_once "/CashbookModel.php";

class CashbookSingularModel extends CashbookModel {
	protected $SectionSelected; // This determines whether the receipt or disbursement section is currently being displayed.
	
	// Fetches database information needed for this model and returns the query results.
	protected function FetchDBInfo() {
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$BookResult_Receipt = $Query->GetReceiptQueryForBook($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Disburse = $Query->GetDisbursements($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Payment = $Query->GetPaymentQueryForBook($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Config = $Query->GetCashbookConfiguration($this->Menu_BookSelected);
		$BroughtForwardResult = $Query->GetAmountBroughtForward($this->Menu_BookSelected, $this->Menu_TargetMonth);

		return array('BookResult_Config' => $BookResult_Config, 'BookResult_Receipt' => $BookResult_Receipt, 'BookResult_Disburse' => $BookResult_Disburse, 'BookResult_Payment' => $BookResult_Payment, 'BroughtForwardResult' => $BroughtForwardResult);
	}
	
	// Parses data retrieved from the database and stores it in the DisburseData array. This is each disbursed payment found for the month.
	protected function ParseDisbursementInfo($BookResult_Disburse) {
		$this->DisburseData = array(); // Initialize array.
		
		if (mysqli_num_rows($BookResult_Disburse) > 0) {
			// Create an array for each receipt.
			while ($row = $BookResult_Disburse->fetch_assoc()) {
				$this->DisburseData[$row['DisbursementUID']] = array('FeeID' => $row['FeeID'], 'Amount' => $row['Amount'], 'CheckNum' => $row['CheckNum'], 'TaxWarrantNum' => $row['TaxWarrantNum'],
																	 'PayTo' => $row['PayTo'], 'ReceivedFor' => $row['ReceivedFor'], 'DateReceived' => $row['DateReceived']);
			}
		} else { // Query found nothing.
			// This error does not need to be echoed in production, as 0 records may simply indicate that nothing has been entered yet this month.
			//echo "Error 223 - Could not load disbursement data for book.";
		}
	}
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// Displays the view tab at the top of the page, allowing users to switch between available views.
	public function DisplayViewMode() {
		echo "<table class='ViewPanel'><tr><th>View: </th>" .
			 "<td><button class='ViewInactive' name='View' onclick='SwitchView()' value='Side'>Side by Side</button></td>" .
			 "<td><button class='ViewInactive' name='View' onclick='SwitchView()' value='Compact'>Compact</button></td>" .
			 "<td>Singular</td></tr></table>";
	}
	
	// Displays the first row of the table, separating the table into 3 sections.
	public function DisplaySectionHeader() {
		if ($this->SectionSelected === 'Disbursement') {
			echo "<th class='TX' colspan='5'><th class='DBS' colspan='7'>DISBURSEMENTS</th>"
				 . "<th class='RTS' colspan='4'><button class='RecSect' name='Section' onclick='SwitchSection()' value='Receipt'>RECEIPTS</button></th>";
				 
		} else { // Default to receipt if any invalid value was stored in $SectionSelected.
			echo "<th class='TX' colspan='5'><th class='RTS' colspan='7'>RECEIPTS</th>"
				 . "<th class='DBS' colspan='4'><button class='DisbSect' name='Section' onclick='SwitchSection()' value='Disbursement'>DISBURSEMENTS</button></th>";
		}
		
	}
	
	// Displays the receipt data column headers.
	public function DisplayReceiptHeader() {
		if ($this->SectionSelected === 'Disbursement') { // Display disbursement category.
			echo "<tr><td class='TXS'>DATE </br> (MM-DD)</td><td class='TXS'>CHECK NUMBER</td> <td class='TXS'>TAX WARRANT NUMBER</td> "
			 . "<td class='OWAS'>TO WHOM PAID</td> <td class='OWAS'>ON WHAT ACCOUNT</td>";
		} else { // Receipt section. Default here if any bad selection is somehow entered.
			echo "<tr><td class='TXS'>DATE </br> (MM-DD)</td><td class='TXS'>RECEIPT NUMBER</td> <td class='TXS'>CAUSE NUMBER</td> "
			 . "<td class='OWAS'>FROM WHOM RECEIVED</td> <td class='OWAS'>ON WHAT ACCOUNT</td>";
		}
	}
	
	// Display the receipt or disbursement header rows of the cashbook. The column names are configurable and so must be looked up in the configuration table.
	public function DisplayInput_ReceiptDisburesmentHeader(){
		if ($this->SectionSelected === 'Disbursement') { // Display disbursement category.
			for ($i = 1; $i <=11; $i++) // Displays columns 1-11 (all columns).
				echo "<td class='DBS'>" . $this->BookConfig["DisburseLabelCol" . $i] . "</td>";
		} else { // Receipt section. Default here if any bad selection is somehow entered.
			for ($i = 1; $i <=11; $i++) // Displays columns 1-11 (all columns).
				echo "<td class='RTS'>" . $this->BookConfig["ReceiptLabelCol" . $i] . "</td>";
		}
	}
	
	// Displays the "Amount Brought Forward" row. This is overwritten by this child model so it can be hidden when the disbursement section is displayed.
	public function Display_AmountBroughtForward() {
		if ($this->SectionSelected === 'Disbursement') {
			// Don't display the amount brought forward on the disbursement page, only for receipts.
			// We default to receipts if SectionSelected is unrecognized, so follow that pattern here.
		} else {
			// Begin the row.
			echo "<tr><th class='OWAS' colspan='5'>Amount Brought Forward</th><td class='RTS'>--</td>";
			
			// For each fee type in the receipt columns, display the amount left over from the previous month.
			for ($i = 2; $i <= 11; $i++) {
				$FeeID = $this->BookConfig["ReceiptValueCol" . $i]; // This is the fee type that should appear in the current column.
				if (array_key_exists($FeeID, $this->BroughtForward)){
					echo "<td class='RTS'>" . $this->BroughtForward[$FeeID] . "</td>";
				} else {
					echo "<td class='RTS'>--</td>";
				}
			}
		}		
	}
	
	// Displays the receipt info for the selected month and cashbook. Orders data to match the column names as configured by the user.
	public function DisplayInput_ReceiptDisbursementData () {
		if ($this->SectionSelected === 'Disbursement') { // Display disbursement category.
			$this->DisplayInput_DisbursementData();
		} else { // Receipt section. Default here if any bad selection is somehow entered.
			$this->DisplayInput_ReceiptData();
		}
	}
	
	// Displays the enlarged Disbursement section unique to the Singular view mode.
	protected function DisplayInput_DisbursementData() {
		// Display each receipt found and the consolidated disbursement data that goes with it.
		foreach ($this->DisburseData as $DisbID => $Disbursement) {
			// Taxpayer info section.
			echo "<tr><td class='TXS'>" . substr($Disbursement["DateReceived"], 5, 5) . "</td><td class='TXS'>" . $Disbursement['CheckNum'] . "</td><td class='TXS'>" . $Disbursement["TaxWarrantNum"] . 
					"</td><td class='OWAS'><div class='scrollable'>". $Disbursement["PayTo"] . "</div></td><td class='OWAS'><div class='scrollable'>" . $Disbursement["ReceivedFor"] . "</div></td>";
			
			// Disbursement section. Recall that each cashbook cell is defined by a receipt type (row) and fee type (column).
			  // First column, total disbursement on this receipt.
			echo "<td class='DBS'>" . $Disbursement["Amount"] . "</td>";
			
			  // Columns 2-11, as configured by user.
			for ($i = 2; $i <= 11; $i++) {
				if ($this->BookConfig["DisburseValueCol" . $i] == $Disbursement["FeeID"]) // TRUE if this disbursements fee type matches the current column's fee tyoe.
					echo "<td class='DBS'>" . $Disbursement["Amount"] . "</td>";
				else // Nothing to display for this cell.
					echo "<td class='DBS'>--</td>";
			}
			
			// End the row.
			echo "</tr>";
		}
	}
	
	// Displays the enlarged Receipt section unique to the Singular view mode.
	protected function DisplayInput_ReceiptData() {
		// Display each receipt found and the payment data that goes with it.
		foreach ($this->ReceiptData as $RID => $Receipt) {
			
			// Taxpayer info section.
			echo "<tr><td class='TXS'>" . substr($Receipt["DateReceived"], 5, 5) . "</td><td class='TXS'>" . $RID . "</td><td class='TXS'>" .
					 $Receipt["CauseNum"] . "</td><td class='OWAS'><div class='scrollable'>". $Receipt["ReceivedOf"] .
					 "</div></td><td class='OWAS'><div class='scrollable'>" . $Receipt["ReceivedFor"] . "</div></td>";
					 
			// Receipt section. Show payments made on a receipt (row) for each receipt type (column), where columns are configured by the user.
			  // Receipt column 1.
			echo "<td class='RTS'>" . $Receipt['TotalCashReceived'] . "</td>";
			  // Receipt columns 2-11.
			for ($i = 2; $i <= 11; $i++) {
				$FeeForThisCol = $this->BookConfig["ReceiptValueCol" . $i]; // This is the fee type that should appear in the current column.

				$ReceiptContainsFeeType = array_key_exists($FeeForThisCol, $this->PaymentData[$RID]); // TRUE if this receipt had a payment with the specified fee type.
				
				if ($ReceiptContainsFeeType) {
					// First we create a copy the entry in the array to make the following statements less verbose.
					$PaymentArray = $this->PaymentData[$RID][$FeeForThisCol];
					
					// Receipt contains a payment with the specified fee type, now determine whether it has been fully disbursed.
					if ($PaymentArray['PaidSoFar'] >= $PaymentArray['Amount']) // Payment has been disbursed, just display the amount (now 0).
						echo "<td class='RTS'>" . $PaymentArray['Amount'] . "</td>";
					else if ($PaymentArray['PaidSoFar'] > 0)// Payment has been disbursed in part, display the amount remaining to be disbursed.
						echo "<td class='RTS'><button class='cashpart' name='PayID' onclick='BindCommandAndSubmit(" . $i . ")' value='" . $PaymentArray['PaymentUID'] . "'>" . $PaymentArray['Amount'] . "</button></td>";
					else // Payment has not yet been disbursed at all. Display the full amount of the payment.
						echo "<td class='RTS'><button class='cashbook' name='PayID' onclick='BindCommandAndSubmit(" . $i . ")' value='" . $PaymentArray['PaymentUID'] . "'>" . $PaymentArray['Amount'] . "</button></td>";
				} else // This receipt had no payment with the specified fee type.
					echo "<td class='RTS'>0</td>";
			}
			
			// End the row.
			echo "</tr>";
		}
	}
}
?>