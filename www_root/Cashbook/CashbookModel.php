<?php
/*
 This file serves as a standard MVC model for cashbooks. Displays data in "Compact" view. Disbursements are consolidated into one lump sum, organized by the receipt the money is being disbursed from.
 */
require_once "../../Includes/Queries.php";
 
class CashbookModel {		
	/* ------- -------- ------- ------- -------- ------- ------- Database query results ------- -------- ------- ------- -------- ------- ------- */
	public $BookConfig = array(); // Array stores information on how this book has been configured by the user. It contains two kinds of entries, Labels and FeeTypes.
								  // Each column in the table can have a label assigned to it and the FeeType of the data that will appear in this column.
								  // These are split between Receipt and Disburse section labels/feetypes. --> ReceiptLabelColX/ReceiptValueColX, DisburseLabelColX/DisburseValueColX
	protected $PaymentData; // Array of arrays. First array is indexed by a line number, subarray contains information for the payment to be displayed on that line.
	protected $DisburseData; // Array of arrays. First array is indexed by ReceiptID, second array contains key:value => FeeID:SumOfDisbursementsMade.
	protected $BroughtForward; // Array of ColNum => Amount. Stores the amount of un-disbursed payments brought forward from the previous month.
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input variables from user ------- -------- ------- ------- -------- ------- ------- */
	public $Menu_BookSelected; // The book selected by the user.
	public $Menu_TargetMonth; // The month selected by the user.
	
	
	// Constructor sets query result parameters.
	function __construct($ParamArray = array()) {
		// Set each parameter in the array.
		foreach($ParamArray as $key=>$value) {
			$this->$key = $value;
		}
		
		// Connect to the database and retrieve the data we need for this model.
		$ResultsArray = $this->FetchDBInfo();
		
		// Now parse the data retrieved into arrays in a format usable by the model.
		$this->ParseConfigInfo($ResultsArray['BookResult_Config']);
		$this->ParseDisbursementInfo($ResultsArray['BookResult_Disburse']);
		$this->ParsePaymentInfo($ResultsArray['BookResult_Payment']);
		$this->ParseAmountBroughtForward($ResultsArray['BroughtForwardResult']);
	}
	
	// Fetches database information needed for this model and returns the query results.
	// This function is defined by each child that extends this model. How the disbursement info is displayed is the primary difference between the different view modes
	// displayed by each child model, so each child retrieves some different information from the database regarding disbursements. Much of the other data is the same.
	protected function FetchDBInfo() {}
	
	// Parses data retrieved from the database and stores it in the BookConfig array.
	protected function ParseConfigInfo($ConfigResult) {
		if (mysqli_num_rows($ConfigResult) > 0) {
			// The row returned has a label and a value for each column.
			$row = $ConfigResult->fetch_assoc();
			
			// Iterate over the row and set each label and data value to the one found.
			// These will have the form: BookConfig['ReceiptLabelColX'] and BookConfig['DisburseLabelColX']
			//							 BookConfig['ReceiptValueColX'] and BookConfig['DisburseValueColX']
			foreach ($row as $key=>$value)
				$this->BookConfig[$key] = $value;
			
		} else { // Query found nothing
			echo "Error 220 - Could not load configuration data for book.";
		}
	}
	
	// Parses data retrieved from the database and stores it in the DisburseData array. This is each disbursed payment found for the month.
	// This function is defined by each child that extends this model. How the disbursement info is displayed is the primary difference
	// between the view modes each child model displays, so the data structure built by this function is different in each child model.
	protected function ParseDisbursementInfo($BookResult_Disburse) {}
	
	// Parses data retrieved from the database and stores it in the PaymentData array.
	protected function ParsePaymentInfo($BookResult_Payment){
		$this->PaymentData = array(); // Initialize array.
		
		// Sort each of the payments into an array ordered by line number
		if (mysqli_num_rows($BookResult_Payment) > 0) {
			while ($row = $BookResult_Payment->fetch_assoc()) {
				$this->PaymentData[$row['cbLineNumber']] = array('ReceiptNum' => $row['ReceiptNum'], 'CauseNumber' => $row['CauseNumber'], 'FromWhomReceived' => $row['FromWhomReceived'], 'OnWhatAccount' => $row['OnWhatAccount'],
																'TotalCashReceived' => $row['TotalCashReceived'], 'TotalCashDisbursed' => $row['TotalCashDisbursed'], 'Col_R_1' => $row['Col_R_1'], 'Col_R_2' => $row['Col_R_2'],
																'Col_R_3' => $row['Col_R_3'], 'Col_R_4' => $row['Col_R_4'], 'Col_R_5' => $row['Col_R_5'], 'Col_R_6' => $row['Col_R_6'], 'Col_R_7' => $row['Col_R_7'],
																'Col_R_8' => $row['Col_R_8'], 'Col_R_9' => $row['Col_R_9'], 'Col_R_10' => $row['Col_R_10'], 'DateReceived' => $row['DateReceived']);
			}
			ksort($this->PaymentData);
		} else { // Query found nothing.
			// This error does not need to be echoed in production, as 0 records may simply indicate that nothing has been entered yet this month.
			//echo "Error 222 - Could not load payment data for book.";
		}
	}
	
	// Parses data retrieved from the database and stores it in the BroughtForward array. This is the amount of un-disbursed payments received for each fee type.
	protected function ParseAmountBroughtForward($BroughtForwardResult) {
		$this->BroughtForward = array(); // Initialize array.
		
		if (mysqli_num_rows($BroughtForwardResult) > 0) {
			$row = $BroughtForwardResult->fetch_assoc();
			$this->BroughtForward = array('1' => $row['cbColBalFwd1'], '2' => $row['cbColBalFwd2'], '3' => $row['cbColBalFwd3'], '4' => $row['cbColBalFwd4'], '5' => $row['cbColBalFwd5'],
											'6' => $row['cbColBalFwd6'], '7' => $row['cbColBalFwd7'], '8' => $row['cbColBalFwd8'], '9' => $row['cbColBalFwd9'], '10' => $row['cbColBalFwd10']);
		}
	}	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// Displays the view tab at the top of the page, allowing users to switch between available views.
	// This function is defined by each child that extends this model.
	public function DisplayViewMode() {}
	
	// Displays the receipt data column headers.
	// This function is defined by each child that extends this model, as the headers are different depending on the view.
	public function DisplayReceiptHeader() {}
	
	// Display the receipt and disbursement header rows of the cashbook. The column names are configurable and so must be looked up in the configuration table.
	public function DisplayInput_ReceiptDisburesmentHeader(){
		// Receipt section.
		for ($i = 1; $i <=11; $i++) // Displays columns 1-11 (all columns).
			echo "<td class='RT'>" . $this->BookConfig["ReceiptLabelCol" . $i] . "</td>";
		
		// Add a small divider between the Receipt and Disbursement sections.
		echo "<td class='smallgap'></td>";
		
		// Disbursement section.
		for ($i = 1; $i <=11; $i++) // Displays columns 1-11 (all columns).
			echo "<td class='DB'>" . $this->BookConfig["DisburseLabelCol" . $i] . "</td>";
	}
	
	// Displays the "Amount Brought Forward" row.
	public function Display_AmountBroughtForward() {
		// Begin the row.
		echo "<tr><th class='OWA' colspan='5'>Amount Brought Forward</th><td class='RT'>--</td>";
		
		// For each column, display the amount left over from the previous month.
		for ($i = 1; $i < 11; $i++) {
			if ($this->BroughtForward[$i] > 0) { // Display blank cell if the amount is 0.
				echo "<td class='RT'>--</td>";
			} else { // Display clickable cell if amount is greater than 0.
				echo "<td class='RT'><button class='cashbook' name='FeeBroughtForward' value='$i' onclick='DisbCol()'>" . $this->BroughtForward[$i] . "</button></td>";
			}
		}
		
		// Display a blank row under the disbursement section and end the table row tag.
		echo "<td colspan='11'></td></tr>";
	}
	
	// Displays the receipt info for the selected month and cashbook. Orders data to match the column names as configured by the user.
	// This function is defined by each child that extends this model. How this data is displayed is the primary difference between the "view modes" each child displays.
	public function DisplayInput_ReceiptDisbursementData () {}
	
	// Displays two hidden inputs that remember the book and month selected by the user, and one that stores the column number that a payment appeared in when the user disburses it.
	public function DisplayInput_HiddenData() {
		echo "<input type='hidden' id='MenuBookTitle' name='MenuBookTitle' value='" . $this->Menu_BookSelected . "' />" .
			 "<input type='hidden' id='TargetMonth' name='TargetMonth' value='" . $this->Menu_TargetMonth . "' />" .
			 "<input type='hidden' id='command' name='command' value='' />" .
			 "<input type='hidden' id='RecCol' name='RecCol' value='' />" . // When a user disburses a payment, we need the column number it appears in to display some help text on the next page.
			 "<input type='hidden' id='RevFee' name='RevFee' value='' />" . // Used when a user expands the consolidated disbursement data on the compact view.
			 "<input type='hidden' id='ColAmount' name='ColAmount' value='' />" . // Used when a user disburses an entire column.
			 "<input type='hidden' id='ReviewCol' name='ReviewCol' value='' />"; // Used when a user reviews the disbursements consolidated into one enty on the compact view (orange buttons).
	}
}
?>