<?php
/*
 This file serves as a standard MVC model for cashbooks. Displays data in "Side by Side" view. Disbursements and Receipts are entirely seperated in this view.
 */
require_once "../../Includes/Queries.php";
require_once "/CashbookModel.php";

class CashbookSideModel extends CashbookModel {
	// Fetches database information needed for this model and returns the query results.
	protected function FetchDBInfo() {
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$BookResult_Disburse = $Query->GetDisbursementDataForBook($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Payment = $Query->GetPaymentDataForBook($this->Menu_BookSelected, $this->Menu_TargetMonth);
		$BookResult_Config = $Query->GetCashbookConfiguration($this->Menu_BookSelected);
		$BroughtForwardResult = $Query->GetAmountBroughtForward($this->Menu_BookSelected, $this->Menu_TargetMonth);

		return array('BookResult_Config' => $BookResult_Config, 'BookResult_Receipt' => $BookResult_Receipt, 'BookResult_Disburse' => $BookResult_Disburse,
					'BookResult_Payment' => $BookResult_Payment, 'BroughtForwardResult' => $BroughtForwardResult);
	}
	
	// Parses data retrieved from the database and stores it in the DisburseData array. This is each disbursed payment found for the month.
	protected function ParseDisbursementInfo($BookResult_Disburse) {
		$this->DisburseData = array(); // Initialize array.
		
		// Sort each of the disbursements into an array ordered by line number
		if (mysqli_num_rows($BookResult_Payment) > 0) {
			while ($row = $BookResult_Payment->fetch_assoc()) {
				$this->DisburseData[$row['cbLineNumber']] = array('CheckNum' => $row['CheckNum'], 'TaxWarrantNumber' => $row['TaxWarrantNumber'], 'ToWhomPaid' => $row['ToWhomPaid'], 'OnWhatAccount' => $row['OnWhatAccount'],
																'TotalCashReceived' => $row['TotalCashReceived'], 'TotalCashDisbursed' => $row['TotalCashDisbursed'], 'Col_D_1' => $row['Col_D_1'], 'Col_D_2' => $row['Col_D_2'],
																'Col_D_3' => $row['Col_D_3'], 'Col_D_4' => $row['Col_D_4'], 'Col_D_5' => $row['Col_D_5'], 'Col_D_6' => $row['Col_D_6'], 'Col_D_7' => $row['Col_D_7'],
																'Col_D_8' => $row['Col_D_8'], 'Col_D_9' => $row['Col_D_9'], 'Col_D_10' => $row['Col_D_10'], 'DateReceived' => $row['DateReceived']);
			}
			ksort($this->DisburseData);
		} else { // Query found nothing.
			// This error does not need to be echoed in production, as 0 records may simply indicate that nothing has been entered yet this month.
			//echo "Error 223 - Could not load payment data for book.";
		}
	}
	
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// Displays the view tab at the top of the page, allowing users to switch between available views.
	public function DisplayViewMode() {
		echo "<table class='ViewPanel'><tr><th>View: </th>" .
			 "<td>Side by Side</td>" .
			 "<td><button class='ViewInactive' name='View' onclick='SwitchView()' value='Compact'>Compact</button></td>" .
			 "<td><button class='ViewInactive' name='View' onclick='SwitchView()' value='Singular'>Singular</button></td></tr></table>";
	}
	
	// Displays the first row of the table, separating the table into 3 sections.
	public function DisplaySectionHeader() {
		echo "<th class='TX' colspan='5'><th class='RT' colspan='11'>RECEIPTS</th><td class='smallgap'></td><th class='DB' colspan='11'>DISBURSEMENTS</th>";
	}
	
	// Displays the receipt data column headers.
	public function DisplayReceiptHeader() {
		echo "<tr><td class='TX'>DATE </br> (MM-DD)</td><td class='TX'>RECEIPT OR CHECK NUMBER</td> <td class='TX'>CAUSE OR TAX WARRANT NUMBER</td> "
			 . "<td class='OWA'>FROM WHOM RECEIVED OR TO WHOM PAID</td> <td class='OWA'>ON WHAT ACCOUNT</td>";
	}
	
	// Displays the receipt info for the selected month and cashbook. Orders data to match the column names as configured by the user.
	public function DisplayInput_ReceiptDisbursementData () {
		
		// Display each receipt found and the payment data that goes with it.
		foreach ($this->PaymentData as $LineNum => $Payment) {
			/*
				$Payment is an array that contains the following information:
				ReceiptNum, CauseNumber, FromWhomReceived, OnWhatAccount,
				Col_R_1, Col_R_2, Col_R_3, Col_R_4, Col_R_5, Col_R_6, Col_R_7, Col_R_8, Col_R_9, Col_R_10
			*/
			
			// Taxpayer info section.
			echo "<tr><td class='TX'>" . substr($Payment["DateReceived"], 5, 5) . "</td><td class='TX'>" . $Payment["ReceiptNum"] . "</td><td class='TX'>" .
					 $Payment["CauseNumber"] . "</td><td class='OWA'><div class='scrollable'>". $Payment["FromWhomReceived"] .
					 "</div></td><td class='OWA'><div class='scrollable'>" . $Payment["OnWhatAccount"] . "</div></td>";
					 
			// Receipt section. Show payments made on a receipt (row) for each receipt type (column), where columns are configured by the user.
			 // Receipt column 1.
			echo "<td class='RT'>" . $Payment['TotalCashReceived'] . "</td>";
			 // Receipt columns 2-11.
			for ($i = 2; $i <= 11; $i++) {
				// Get the value for this column.
				$ColValue = $Payment['Col_R_' . ($i - 1)];
				
				if ($ColValue == 0) { // Nothing to see here, move along.
					echo "<td class='RT'>0</td>";
					
				} else { // Something needs to be displayed.
					// Display a cell with a value in it. Depending on whether this column has been disbursed, it may be interactable.
					if ($Payment['TotalCashDisbursed'] == 0) { // Nothing has been disbursed yet.
						echo "<td class='RT'><button class='cashbook' name='LineNum' onclick='BindCommandAndSubmit(" . $i . ")' value='" . $LineNum . "'>" . $ColValue . "</button></td>";
						
					} else if ($Payment['TotalCashReceived'] > $Payment['TotalCashDisbursed']) { // Entry has only been partially disbursed..
						echo "<td class='RT'><button class='cashpart' name='LineNum' onclick='BindCommandAndSubmit(" . $i . ")' value='" . $LineNum . "'>" . $ColValue . "</button></td>";
						
					} else { // Payment has been fully disbursed.
						echo "<td class='RT'>" . $ColValue . "</td>";
					}
				}
			}
			
			// Add a small divider between the Receipt and Disbursement sections.
			echo "<td class='smallgap'></td>";
			
			// Disbursement section.
			for ($i = 1; $i <= 11; $i++)
				echo "<td class='DB'>--</td>";
			
			// End the row.
			echo "</tr>";
		}
		
		// Display each receipt/disbursement found that goes with it.
		foreach ($this->DisburseData as $LineNum => $Disbursement) {
			/*
				$Disbursement is an array containing the following elements:
				CheckNum, DateReceived, TaxWarrantNumber, ToWhomPaid, OnWhatAccount, TotalCashReceived, TotalCashDisbursed,
				Col_D_1, Col_D_2, Col_D_3, Col_D_4, Col_D_5, Col_D_6, Col_D_7, Col_D_8, Col_D_9, Col_D_10
			*/
			
			// Taxpayer info section.
			echo "<tr><td class='TX'>" . substr($Disbursement["DateReceived"], 5, 5) . "</td><td class='TX'>" . $Disbursement['CheckNum'] . "</td><td class='TX'>" . $Disbursement["TaxWarrantNumber"]
					. "</td><td class='OWA'><div class='scrollable'>". $Disbursement["ToWhomPaid"] . "</div></td><td class='OWA'><div class='scrollable'>" . $Disbursement["OnWhatAccount"] . "</div></td>";

			// Receipt section.
			for ($i = 1; $i <= 11; $i++)
				echo "<td class='RT'>--</td>";
			
			// Add a small divider between the Receipt and Disbursement sections.
			echo "<td class='smallgap'></td>";
			
			// Disbursement section. Recall that each cashbook cell is defined by a receipt type (row) and fee type (column).
			 // First column, total disbursement on this receipt.
			echo "<td class='DB'>" . $Disbursement["Amount"] . "</td>";
			
			 // Columns 2-11, as configured by user.
			for ($i = 2; $i <= 11; $i++) {
				// Get the value for the current cell.
				$CellValue = $Disbursement['Col_D_' . ($i - 1)];
				
				// Display cell value.
				if ($CellValue == 0) { // Nothing to display.
					echo "<td class='DB'>--</td>";
				} else {
					echo "<td class='DB'>" . $CellValue . "</td>";
				}
			}
			
			// End the row.
			echo "</tr>";
		}
	}
}
?>