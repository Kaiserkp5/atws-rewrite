<?php
/*
 Controller for the cashbook system.
*/
// Authenticate user.
require_once("../../Includes/authenticator.php");

// Create the page template
require_once('../lib/PageTemplate.php');
if (!isset($TPL)) {
    $TPL = new PageTemplate(array('PageTitle' => "Cash Book", 'ContentBody' => __FILE__, 'FooterMedia' => "", 'Copyright' => "Copyright (c) Lieberman Technologies, LLC.",
								  'ScriptFile' => REALPATH(DIRNAME(__FILE__)) . "/Cashbook.script"));
    require "../lib/layout.php";
	exit;
}

// Load models.
require_once "/CashbookCompactModel.php";
require_once "/CashbookSingularModel.php";
require_once "/CashbookSideModel.php";
require_once "/CashbookMenuModel.php";
require_once "/DisburseModel.php";
require_once "/DisburseReviewModel.php";
require_once "/DisburseColumnModel.php";

// Controller functionality.
if (isset($_POST['command'])) { // The user entered some command.
	// Determine the command entered and act accordingly.
	switch ($_POST['command']) {// User loaded a cashbook.
		case "Submit":
			// Build the model using supplied input.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			
			$model = new CashbookSideModel($ParamArray);
			require_once('Cashbook.html');
			break;
		case "New": // User is adding a new month entry to the selected cashbook.
			// Create the menu model.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$model = new CashbookMenuModel($ParamArray);
			
			// Add the new month.
			$model->AddNew();
			
			// Display the new menu.
			require_once('CashbookMenu.html');
			break;
		case "disburse": // Return a menu allowing the user to disburse the selected payment.
			// Build the model using supplied input.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			$ParamArray['DisburseDate'] = date("Y-m-j");
			$ParamArray['LineNum'] = $_POST['LineNum'];
			$ParamArray['CBcol'] = $_POST['RecCol'];
			
			$model = new DisburseModel($ParamArray);
			require_once('DisbursePayment.html');
			break;
		case "DisbColumn": // Return a view that allows the user to disburse the selected column.
			// Build the model using supplied input.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			$ParamArray['DisburseDate'] = date("Y-m-j");
			$ParamArray['ColumnBroughtForward'] = $_POST['FeeBroughtForward'];
			
			$model = new DisburseColumnModel($ParamArray);
			require_once('DisburseColumn.html');
			break;
		case "Print": // Disburse the selected payment and reload the cashbook.
			// First build the model.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			$ParamArray['DisburseDate'] = date("Y-m-j");
			$ParamArray['LineNum'] = $_POST['LineNum'];
			$ParamArray['DisburseAmount'] = $_POST['DisburseAmount'];
			$ParamArray['PayTo'] = $_POST['PayTo'];
			$ParamArray['PayFor'] = $_POST['PayFor'];
			$ParamArray['DisburseTo'] = $_POST['DisburseTo'];
			$ParamArray['TaxWarrantNum'] = $_POST['TaxWarrantNum'];
			$ParamArray['CauseNum'] = $_POST['CauseNum'];
			$ParamArray['CBcol'] = $_POST['RecCol'];
			$model = new DisburseModel($ParamArray);
			
			$ValMsg = $model->IsValid();
			if ($ValMsg === "VALID") { // If the model is valid, disburse it.
				$model->DisbursePayment();
				
				// Build the cashbook model using supplied input and display the cashbook.
				$ParamArray = array();
				$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
				$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
				
				$model = new CashbookSideModel($ParamArray);
				require_once('Cashbook.html');
				
			} else { // Model isn't valid, display an error.
				$model->DisplayErrorMessage($ValMsg);
				require_once('DisbursePayment.html');
			}
			break;
		case "Print Check": // User disbursed a column, iterate over each payment to mark it disbursed and print a check.
			// Build model
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			$ParamArray['DisburseDate'] = date("Y-m-j");
			$ParamArray['DisburseAmount'] = $_POST['DisburseAmount'];
			$ParamArray['PayTo'] = $_POST['PayTo'];
			$ParamArray['PayFor'] = $_POST['PayFor'];
			$ParamArray['DisburseTo'] = $_POST['DisburseTo'];
			$ParamArray['TaxWarNum'] = $_POST['TaxWarrantNum'];
			$ParamArray['CauseNum'] = $_POST['CauseNum'];
			$ParamArray['CBcolumn'] = $_POST['RecCol'];
			$ParamArray['ColumnBroughtForward'] = $_POST['ColForward'];
			$ParamArray['FullAmount'] = $_POST['FullAmount'];
			$model = new DisburseColumnModel($ParamArray);
			
			// Check Validity
			$ValMsg = $model->IsValid();
			
			// Disburse or display an error
			if ($ValMsg === "VALID") {
				$DBmsg = $model->DisbursePayments();
				
				if ($DBmsg === 'SUCCESS') {
					// Redirect back to the cashbook page.
					$NewParams = array();
					$NewParams['Menu_BookSelected'] = $_POST['MenuBookTitle'];
					$NewParams['Menu_TargetMonth'] = $_POST['TargetMonth'];
					$model = new CashbookSideModel($NewParams);
					require_once('Cashbook.html');
					
				} else {
					// Alert the user to the error.
					$model->DisplayErrorMessage($DBmsg);
					require_once('DisburseColumn.html');
				}
			} else {
				$model->DisplayErrorMessage($ValMsg);
				require_once('DisburseColumn.html');
			}
			break;
		case "Cancel": // User canceled the disbursement, return the cashbook page.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			
			$model = new CashbookSideModel($ParamArray);
			require_once('Cashbook.html');
			break;
		case "Refresh Check": // Refresh the check number field on the disbursement page.
			// Build the model using supplied input.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			$ParamArray['DisburseDate'] = date("Y-m-j");
			$ParamArray['LineNum'] = $_POST['LineNum'];
			$ParamArray['CBcol'] = $_POST['RecCol'];
			$ParamArray['DisburseAmount'] = $_POST['DisburseAmount'];
			$ParamArray['PayTo'] = $_POST['PayTo'];
			$ParamArray['PayFor'] = $_POST['PayFor'];
			$ParamArray['DisburseTo'] = $_POST['DisburseTo'];
			$ParamArray['DisburseTaxWarrant'] = $_POST['TaxWarrantNum'];
			$ParamArray['DisburseCauseNum'] = $_POST['CauseNum'];
			
			$model = new DisburseModel($ParamArray);
			require_once('DisbursePayment.html');
			break;
		case "review": // Display a page that shows the disbursements that went into this columns total.
			// Build the model using supplied input.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			$ParamArray['ReceiptID'] = $_POST['DisRec'];
			$ParamArray['FeeID'] = $_POST['RevFee'];
			$ParamArray['ReviewCol'] = $_POST['ReviewCol'];
			
			$model = new DisburseReviewModel($ParamArray);
			require_once('DisburseReview.html');
			break;
		case "BookChange": // Update the menu with the newly selected cashbook.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			
			$model = new CashbookMenuModel($ParamArray);
			require_once('CashbookMenu.html');
			break;
		case "Back": // User clicked the "Back" button on the disbursement review page, redirect back to the cashbook page.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			
			$model = new CashbookSideModel($ParamArray);
			require_once('Cashbook.html');
			break;
		case "SwitchView": // Switch to the view mode selected by the user.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			
			// Determine the view to switch to.
			$NewView = $_POST['View'];
			switch ($NewView) {
				case 'Side':
					$model = new CashbookSideModel($ParamArray);
					break;
				case 'Compact':
					$model = new CashbookCompactModel($ParamArray);
					break;
				case 'Singular':
					$ParamArray['SectionSelected'] = 'Receipt';
					$model = new CashbookSingularModel($ParamArray);
					break;
				default:
					echo "Error 20 - Unrecognized view mode: " . $_POST['View'];
					$model = new CashbookSideModel($ParamArray);
					break;
				}
			require_once('Cashbook.html');
			break;
		case "SwitchSection": // Switch to the section selected by the user. This relates to the Singular view mode, in which receipts and disbursements are displayed seperately.
			$ParamArray = array();
			$ParamArray['Menu_BookSelected'] = $_POST['MenuBookTitle'];
			$ParamArray['Menu_TargetMonth'] = $_POST['TargetMonth'];
			$ParamArray['SectionSelected'] = $_POST['Section'];
			
			$model = new CashbookSingularModel($ParamArray);
			require_once('Cashbook.html');
			break;
		default:
			echo "Error 10 - Unrecognized command: " . $_POST['command'];
			break;
	}
} else { // No command entered, user must have just navigated to the page. Display the menu.
	// Create the model.
	$ParamArray = array();
	$model = new CashbookMenuModel($ParamArray);
	require_once('CashbookMenu.html');
}
?>