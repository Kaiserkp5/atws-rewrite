<?php
/*
 This file serves as a standard MVC model for disbursements.
 */
require_once "../../Includes/Queries.php";
 
class DisburseColumnModel {	
	/* ------- -------- ------- ------- -------- ------- ------- Database query results ------- -------- ------- ------- -------- ------- ------- */
	private $DisburseToArray; // Array of tuples containing fee titles and IDs. Used to create the 'Disburse To' dropdown box.
	private $ReceiptLabels; // Array containing the labels for each receipt column in the selected cashbook. Array has the form ColumnNum => Label
	private $NextCheck; // The next check number that will be issued if no other check is printed first.
	public $FullAmount; // Maximum amount of money that can be disbursed.
	
	/* ------- -------- ------- ------- -------- ------- ------- Input variables from user ------- -------- ------- ------- -------- ------- ------- */
	/* These values are passed in from the controller. */
	public $Menu_BookSelected; 	//- These two $Menu fields (book and month) represent an entry in the cashbook that the user is viewing. The payment being
	public $Menu_TargetMonth;  	//- disbursed (identified by the ID below) is contained within the cashbook entry identified by this book-month pair.
	public $ColumnBroughtForward; // Column number the user disbursed
	public $DisburseDate; // Date column was disbursed.
	
	/* These values are entered by the user. */
	public $DisburseAmount; // Amount of money user has entered to be disbursed.
	public $DisburseTo; // Fee ID to disburse the money to.
	public $PayTo; // Will store the user entered value for the "Pay to the order of" field
	public $PayFor; // Will store the user entered value for the "For" field
	public $CauseNum; // Will store the user entered cause number, although the actual cause numbers these payments were all entered on may vary.
	public $TaxWarNum; // Will store the user entered tax warrant number, although the actual cause numbers these payments were all entered on may vary.
	
	
	// Constructor sets query result parameters.
	function __construct($ParamArray = array()) {
		// Set each parameter in the array.
		foreach($ParamArray as $key => $value) {
			$this->$key = $value;
		}
		
		// Access the Queries file to obtain database credentials and SQL queries. See the queries.php file itself for more info.
		$Query = new Queries();
		$DisburseToResult = $Query->GetBookConfig($this->Menu_BookSelected); // Query for config data for this book. We need to know what columns are valid to disburse to for this book.
		$NextCheckResult = $Query->NextCheckNum(); // Gets the next check number to be used.
		$ColumnInfoResult = $Query->GetAmountBroughtForward($this->Menu_BookSelected, $this->Menu_TargetMonth); // Gets information about the column being disbursed, such as the maximum amount to disburse.
		
		// Now parse the data retrieved into arrays in a format usable by the model.
		$this->ParseDisbursementInfo($DisburseToResult);
		$this->ParseNextCheckInfo($NextCheckResult);
		$this->ParseColumnInfo($ColumnInfoResult);
	}
	
	// Parses info found in the database regarding valid disbursement options into a format usable by this model.
	public function ParseDisbursementInfo($DisburseResult) {
		$this->DisburseToArray = array(); // Initialize array.
		
		if (mysqli_num_rows($DisburseResult) > 0) {
			// The rows returned has columns labeled columnNumber (1-20) and colHeader (text).
			while ($row = $DisburseResult->fetch_assoc();) {
				if ($row['columnNumber'] < 11) { // Receipt columns
					$this->ReceiptLabels[$row['columnNumber']] = $row['colHeader'];
				} else { // Disbursement columns
					$this->DisburseToArray[$row['columnNumber']] = $row['colHeader'];
				}
			}
			
		} else { // Query found nothing.
			echo "Error 520 - Could not load disbursement data for book.";
		}
	}
	
	// Parses the next check number found in the database.
	public function ParseNextCheckInfo($NextCheckResultSet) {
		if (mysqli_num_rows($NextCheckResultSet) > 0) {
			$row = $NextCheckResultSet->fetch_assoc(); // Get the row from this query (there's only one).
			$this->NextCheck = ($row['CheckNum'] === NULL ? 1 : $row['CheckNum']);
		} else { // Query for NextCheck found nothing.
			echo "Error 540 - Next check number could not be determined.";
		}
	}
	
	// Parses the column info found in the database.
	public function ParseColumnInfo($ColumnResult) {
		if (mysqli_num_rows($NextCheckResultSet) > 0) {
			$row = $ColumnResult->fetch_assoc(); // Get the row from this query (there's only one).
			$this->FullAmount = $row['cbColBalFwd' . $this->ColumnBroughtForward];
		} else { // Query for NextCheck found nothing.
			echo "Error 590 - Could not load amount brought forward for the given month.";
		}
	}
	
	// Returns "VALID" if the current configuration of this model represents a valid disbursement ready to be inserted into the database and an error message otherwise.
	public function IsValid() {
		// Return an error if any required fields are empty.
		if (empty($this->ColumnBroughtForward)) { return "Error 550 - Fee ID to disburse could not be found."; }
		if (empty($this->DisburseTo)) { return "Error 560 - Could not load Disburse To column."; }
		if (empty($this->DisburseAmount)) { return "Please enter an amount to disburse."; }
		if (empty($this->DisburseDate)) { return "Error 570 - Could not load Disburse Date."; }
		if (empty($this->NextCheck)) { return "Error 580 - Could not load next check number."; }
		if (empty($this->PayTo)) { return "Pay to the order of field cannot be empty." ;}
		if (empty($this->PayFor)) { return "For field cannot be empty."; }
		
		// User cannot disburse more money than exists, or a non-numeric amount.
		if (!is_numeric($this->DisburseAmount)) { return "Disburse amount must be numeric."; }
		if ($this->DisburseAmount > $this->FullAmount) { return "The maximum amount that can be disbursed on this payment is $" . $this->FullAmount; }
		
		// Tax warrant and cause number fields???
		
		
		// If nothing was wrong...
		return "VALID";
	}
	
	// Connects to the database and enters a disbursement as defined by the user.
	public function DisbursePayments() {
		
		
		return $Query->();
	}
	
	// Logs the given error text in the given error log file. Creates the file if it does not exist.
	public function LogDBError($ErrorMessage, $ErrorFile) {
		try {
			file_put_contents($ErrorFile, $ErrorMessage . "\r\n", FILE_APPEND | LOCK_EX);
		}
		catch (Exception $e) {
			echo "<script type='text/javascript'>alert('Could not write error to log.')</script>";
		}
	}
	
	// Displays the given error message.
	public function DisplayErrorMessage($Message) {
		echo "<script type='text/javascript'>alert('Failed - " . $Message . "')</script>";
	}
	
	/* ------- -------- ------- ------- -------- ------- ------- Input display functions ------- -------- ------- ------- -------- ------- ------- */
	// This function displays the reminder text at the top of the table, giving the user a few details about the payment they chose to disburse.
	public function DisplayInput_InfoText() {
		echo "<td class='InfoText' colspan='4'>Disbursing up to $" . $this->FullAmount . " from cashbook column '" . $this->ReceiptLabels[$this->ColumnBroughtForward] . "'.</td>";
	}
	
	// This function displays a dropdown that contains each disbursement column that appears in the currently selected cashbook.
	public function DisplayInput_DisburseTo() {
		// Begin the select tag (and other formatting tags for this row).
		echo "<th class='DB'>Disburse to</th><td colspan='2'><select class='DisburseTo' name='DisburseTo' value='" . $this->DisburseTo . "'>";
		
		// Add each option.
		foreach ($this->DisburseToArray as $ID => $ValArray) {
			if (isset($this->DisburseTo) && ($ValArray['Value'] == $this->DisburseTo)) // If a DisburseTo category is already selected (perhaps the form was refreshed), keep it selected.
				echo "<option value='" . $ValArray['Value'] . "' selected>" . $ValArray['Label'] . "</option>";
			else
				echo "<option value='" . $ValArray['Value'] . "'>" . $ValArray['Label'] . "</option>";
		}
		
		// Close off the select tag and formatting tags.
		echo "</select></td><td></td>";
	}
	
	// This function displays a textbox who's value is tied to the DisburseAmount parameter in this model.
	public function DisplayInput_DisburseAmount() {
		echo "<th class='DB'>Amount</th><td><input type='textbox' name='DisburseAmount' value='" . $this->DisburseAmount . "' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the DisburseDate parameter in this model.
	public function DisplayInput_DisburseDate() {
		echo "<th class='DB'>Date</th><td><input type='textbox' value='" . $this->DisburseDate . "' readonly /></td>";
	}
	
	// This function displays a textbox who's value is tied to the cause number parameter in this model.
	public function DisplayInput_CauseNum() {
		echo "<th class='DB'>Cause Number</th><td><input type='textbox' name='CauseNum' value='" . $this->CauseNum . "' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the tax warrant number parameter in this model.
	public function DisplayInput_TWNum() {
		echo "<th class='DB'>Tax Warrant #</th><td><input type='textbox' name='TaxWarrantNum' value='" . $this->TaxWarNum . "' /></td>";
	}
	
	// This function displays a cell containing the next check number to be printed.
	public function DisplayInput_NextCheck() {
		echo "<th class='DB'>Next Check # " . $this->NextCheck . "</th><td><input type='submit' name='command' value='Refresh Check' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the PayTo parameter in this model.
	public function DisplayInput_PayTo() {
		echo "<th class='DB'>Pay to the order of</th><td colspan='3'><input type='textbox' name='PayTo' style='width:100%' value='" . $this->PayTo . "' /></td>";
	}
	
	// This function displays a textbox who's value is tied to the PayFor parameter in this model.
	public function DisplayInput_PayFor() {
		echo "<th class='DB'>For</th><td colspan='3'><input type='textbox' style='width:100%' name='PayFor' value='" . $this->PayFor . "' /></td>";
	}
	
	// Displays two hidden inputs that remember the book and month selected by the user, and one that stores the column number that a payment appeared in when the user disburses it.
	public function DisplayInput_HiddenData() {
		echo "<input type='hidden' id='MenuBookTitle' name='MenuBookTitle' value='" . $this->Menu_BookSelected . "' />" .
			 "<input type='hidden' id='TargetMonth' name='TargetMonth' value='" . $this->Menu_TargetMonth . "' />" .
			 "<input type='hidden' id='ColForward' name='ColForward' value='" . $this->ColumnBroughtForward . "' />" .
			 "<input type='hidden' id='FullAmount' name='FullAmount' value='" . $this->FullAmount . "' />";
	}
}
?>